﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
    public class CarsResponse
    {
    }

    public class CarBrandsResponse
    {
        public int carBrandId { get; set; }
        public string carBrandName { get; set; }
    }

    public class CarSeriesResponse
    {
        public int carBrandId { get; set; }
        public string carBrandName { get; set; }
        public string carSeriesId { get; set; }
        public string carSeriesName { get; set; }
    }
}