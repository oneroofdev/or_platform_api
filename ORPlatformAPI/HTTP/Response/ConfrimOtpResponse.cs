﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
    public class ConfrimOtpResponse
    {
        public string userId { get; set; }
        public bool expired { get; set; }
    }
}