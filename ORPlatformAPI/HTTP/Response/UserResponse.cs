﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
   
    public class LoginResponse
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string userTypeId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }
    
}