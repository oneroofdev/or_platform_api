﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
    public class PersonResponse
    {
        public int personId { get; set; }
        public string personCode { get; set; }
        public int? titileId { get; set; }
        public string idCard { get; set; }
        public string passport { get; set; }
        public string titileName { get; set; }
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string lineId { get; set; }
        public string mobilePhone { get; set; }
        public string mobilePhoneSMS { get; set; }
        public DateTime? birthDate { get; set; }
        public string email { get; set; }
        public DateTime? regisDate { get; set; }
        public string priority { get; set; }
        public string memberTier { get; set; }
        public int? addrTranId { get; set; }
        public string addrType { get; set; }
        public string addrNo { get; set; }
        public string buildingName { get; set; }
        public string floor { get; set; }
        public string village { get; set; }
        public string moo { get; set; }
        public string soi { get; set; }
        public string road { get; set; }
        public int? subDistinctCode { get; set; }
        public string subDistinctName { get; set; }
        public int? distinctCode { get; set; }
        public string distinctName { get; set; }
        public int? provinceCode { get; set; }
        public string provinceName { get; set; }
        public string zipCode { get; set; }
        public string companyName { get; set; }
        public List<PersonCarResponse> cars { get; set; }
        public string carsText { get; set; }
        public string gender { get; set; }
        public string phone { get; set; }
        public string personType { get; set; }

    }
    public class TitlePresonResponse
    {
        public int titleId { get; set; }
        public string titleName { get; set; }
        public string titleNameEN { get; set; }
        public string titleGender { get; set; }
    }
    public class PersonCarResponse
    {
        public int personId { get; set; }
        public string regisNo { get; set; }
        public string series { get; set; }
        public string brandName { get; set; }
        public int tranId { get; set; }
        public string carYear { get; set; }
        public int carProvince { get; set; }
    }


}