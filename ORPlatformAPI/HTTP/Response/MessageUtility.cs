﻿using ORApiFramework.HTTP.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
    public static class MessageUtility
    {
        public static ActionResultStatus ThrowErrorMessage(object model, string msg, string desc = "")
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus
            {
                success = false,
                message = msg,
                description = desc,
                otherData = new
                {
                    input = model
                }
            };
            return actionResultStatus;
        }
    }
}