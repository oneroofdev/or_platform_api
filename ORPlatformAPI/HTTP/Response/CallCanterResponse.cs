﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
    public class CallCanterResponse
    {
    }
    public class QuestionTypeResponse
    {
        public int questionTypeId { get; set; }
        public string questionTypeDesc { get; set; }
    }
    public class BranchResponse
    {
        public int branchId { get; set; }
        public string branchName { get; set; }
        public string area { get; set; }
    }
    public class CallChannelResponse
    {
        public int channelId { get; set; }
        public string channelName { get; set; }
    }
    public class QuestionResponse
    {
        public int questionId { get; set; }
        public string questionDesc { get; set; }
        public int questionTypeId { get; set; }
        public string questionType { get; set; }
        public string questionTypeDesc { get; set; }
        public string isBranch { get; set; }
        public string active { get; set; }
        public string type { get; set; }
        public string createBy { get; set; }
        public DateTime? createDate { get; set; }
        public string updateBy { get; set; }
        public DateTime? updateDate { get; set; }
    }
    public class AnswerResponse
    {
        public int answerId { get; set; }
        public int questionId { get; set; }
        public string answerDesc { get; set; }
        public string followCase { get; set; }
        public string active { get; set; }
    }
    public class DataCallHistoryResponse
    {
        public string trCallHId { get; set; }
        public string trCallDId { get; set; }
        public string name { get; set; }
        public string finishCase { get; set; }
        public string callerId { get; set; }
        public string contactName { get; set; }
        public string contactPhone { get; set; }
        public DateTime? receiveDate { get; set; }
        public string receiveBy { get; set; }
        public string finishCall { get; set; }
        public string questionDesc { get; set; }
        public string answerDesc { get; set; }
        public string personCode { get; set; }
        public string username { get; set; }
        public int? branchId { get; set; }
        public string branchName { get; set; }
        public string area { get; set; }
    }
    public class DataCallHistoryDetailResponse
    {
        public string trCallDId { get; set; }
        public string finishCase { get; set; }
        public DateTime? finishDate { get; set; }
        public string questionDesc { get; set; }
        public string answerDesc { get; set; }
        public string followCase { get; set; }
        public string seriousCase { get; set; }
        public string remarkDet { get; set; }
        public int? branchId { get; set; }
        public string branchName { get; set; }
        public string area { get; set; }
    }

    public class DataCallHistoryDetailByIDResponse
    {
        public string trCallHId { get; set; }
        public string trCallDId { get; set; }
        public string contactName { get; set; }
        public string contactPhone { get; set; }
        public int questionTypeId { get; set; }
        public int questionId { get; set; }
        public int answerId { get; set; }
        public string remarkDet { get; set; }
        public string contactChannel { get; set; }
        public string finishCase { get; set; }
        public string followCase { get; set; }
        public string seriousCase { get; set; }
        public int? branchId { get; set; }
        public string branchName { get; set; }
        public string area { get; set; }
    }
    public class DataSearchCallResponse
    {
        public string trCallHId { get; set; }
        public string trCallDId { get; set; }
        public string name { get; set; }
        public string finishCase { get; set; }
        public string followCase { get; set; }
        public string questionId { get; set; }
        public string questionTypeId { get; set; }
        public string seriousCase { get; set; }
        public string callerId { get; set; }
        public string contactName { get; set; }
        public string contactPhone { get; set; }
        public string receiveDate { get; set; }
        public string receiveBy { get; set; }
        public string finishCall { get; set; }
        public string finishDate { get; set; }
        public string questionDesc { get; set; }
        public string answerDesc { get; set; }
        public string personCode { get; set; }
        public string username { get; set; }
        public string branchId { get; set; }
        public string branchName { get; set; }
        public string area { get; set; }
    }
}