﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Response
{
    public class AddressResponse
    {
    }
    public class ProvinceResponse
    {
        public int provinceCode { get; set; }
        public string provinceName { get; set; }
        public string provinceNameEN { get; set; }
    }
    public class DistrictResponse
    {
        public int provinceCode { get; set; }
        public int districtCode { get; set; }
        public string districtName { get; set; }
        public string districtNameEN { get; set; }
    }
    public class SubDistrictResponse
    {
        public int districtCode { get; set; }
        public int subDistrictCode { get; set; }
        public string subDistrictName { get; set; }
        public string subDistrictNameEN { get; set; }
        public string zipcode { get; set; }
    }
}