﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Request
{
    public class PersonRequest
    {
        public string channel { get; set; }
        public string corpId { get; set; }
        public string corpPersonId { get; set; }
        public string personCode { get; set; }
        public string priority { get; set; }
        public string addressType { get; set; }
        public string memberTier { get; set; }
        public string actionBy { get; set; }

        public string personId { get; set; }
        public string titleId { get; set; }
        public string fnameTh { get; set; }
        public string lnameTh { get; set; }
        public string companyName { get; set; }
        public string personType { get; set; }
        public string gender { get; set; }
        public string idcard { get; set; }
        public string mobile { get; set; }
        public string mobileSMS { get; set; }
        public string phone { get; set; }
        public string birthday { get; set; }
        public string lineId { get; set; }
        public string email { get; set; }
        public string passport { get; set; }

        //address
        public string addrTranId { get; set; }
        public string addressNo { get; set; }
        public string village { get; set; }
        public string floor { get; set; }
        public string moo { get; set; }
        public string road { get; set; }
        public string soi { get; set; }
        public string provinceId { get; set; }
        public string districtId { get; set; }
        public string subdistrictId { get; set; }
        public string zipcode { get; set; }
        public string buildingName { get; set; }


        //cars
        public List<PersonCarsRequest> personCars { get; set; }
    }
    public class PersonCarsRequest
    {
        public string tranId { get; set; }
        public int personId { get; set; }
        public string carBrandName { get; set; }
        public string series { get; set; }
        public string register { get; set; }
        public string active { get; set; }
        public string carProvince { get; set; }
        public string carYear { get; set; }
    }
    public class PersonSearchRequest
    {
        public int corpId { get; set; }
        public int personId { get; set; }
        public string textSearch { get; set; }
        public string typeSearch { get; set; }
    }


}