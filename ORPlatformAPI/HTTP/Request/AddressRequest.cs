﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Request
{
    public class AddressRequest
    {
        public int provinceCode { get; set; }
        public int districtCode { get; set; }
        public int subDistrictCode { get; set; }
    }
    
}