﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Request
{
    public class ConfrimOtpRequest
    {
        public string mobileNo { get; set; }
        public string userName { get; set; }
        public string otp { get; set; }
        public string refCode { get; set; }
    }
}