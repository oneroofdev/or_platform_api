﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Request
{
    public class LoginRequest
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string userTypeId { get; set; }
        public int corpId { get; set; } 
    }
    public class ChangePasswordRequest
    {
        public int userId { get; set; }
        public string password { get; set; }
        public string oldPassword { get; set; }
    }
    public class DataLoginInfo
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public int empId { get; set; }
        public int corpId { get; set; }
        public int deptId { get; set; }
        public string deptName { get; set; }
        public string isExecutive { get; set; }
        public string corpName { get; set; }
        public string corpLogo { get; set; }
        
        public List<DataLoginMenuInfo> dataLoginMenu { get; set; }
    }
    public class DataLoginMenuInfo
    {
        public int programId { get; set; }
        public int displaySeq { get; set; }
        public string menuName { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public int grpMenuId { get; set; }
        public string grpMenuName { get; set; }
        public string iconMenu { get; set; }
        public string displayOnPage { get; set; }
        public string isMain { get; set; }
        public List<SubManu> subMenu { get; set; }
    }
    public class SubManu
    {
        public int displaySeq { get; set; }
        public string menuName { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public int grpMenuId { get; set; }
        public string iconMenu { get; set; }
        public string displayOnPage { get; set; }
    }
}