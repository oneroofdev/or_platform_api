﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Request
{
    public class QARequest
    {
        public string questionId { get; set; }
        public string actionBy { get; set; }
        public string corpId { get; set; }
        public List<AnswerRequest> answer { get; set; }
    }

    public class QuestionRequest
    {
        public string questionId { get; set; }
        public string corpId { get; set; }
        public string questionTypeId { get; set; }
        public string questionDesc { get; set; }
        public string isActive { get; set; }
        public string isBranch { get; set; }
        public string type { get; set; }
        public string actionBy { get; set; }
    }

    public class AnswerRequest
    {
        public string answerId { get; set; }
        public string answerDesc { get; set; }
        public string isFollowCase { get; set; }
        public string active { get; set; }
    }
}