﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.HTTP.Request
{
    public class CallCanterRequest
    {
    }

    public class GetQuestionTypeRequest
    {
        public string userName { get; set; }
        public int corpId { get; set; }

    }
    public class GetCallChannelRequest
    {
        public string userName { get; set; }
    }
    public class GetQuestionRequest
    {
        public int questionType { get; set; }
        public int corpId { get; set; }
        public string type { get; set; }
    }
    public class GetAnswerRequest
    {
        public int questionId { get; set; }
        public int corpId { get; set; }
    }

    public class GetHistoryCallRequest
    {
        public int callId { get; set; }
        public int corpId { get; set; }
        public string isFinishCase { get; set; }
    }

    public class DataCallRequest
    {
        public string trCallDId { get; set; }
        public int? corpId { get; set; }
        public string trCallHId { get; set; }
        public string callerId { get; set; }
        public string callerType { get; set; }
        public string contactName { get; set; }
        public string contactPhone { get; set; }
        public int questionId { get; set; }
        public int answerId { get; set; }
        public string contactChannel { get; set; }
        public string remarkDet { get; set; }
        public string seriousCase { get; set; }
        public string followCase { get; set; }
        public string finishCase { get; set; }
        public int? branch { get; set; }
        public string userName { get; set; }

    }
    public class DataCallSearchRequest
    {
        public string userRecieve { get; set; }
        public int? corpId { get; set; }
        public int questionType { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string seriousCase { get; set; }
        public string followCase { get; set; }
        public string finishCase { get; set; }
        public string userName { get; set; }

    }
}