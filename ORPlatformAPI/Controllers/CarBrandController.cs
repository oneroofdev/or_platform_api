﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORPlatFormAPI.Controllers
{
    public class CarBrandController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "CarBrand";
            }
        }
        [Route("api/getCarBrandList")]
        [HttpGet]
        public IHttpActionResult getCarBrandList([FromUri] AddressRequest model)
        {
            this.MethodName = "getCarBrandList";
            ActionResultStatus responseData = CarsBrandBL.ListCarBrand();
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}