﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;


namespace ORPlatFormAPI.Controllers
{
    public class OTPController : AbApiController
    {
        public override string ModuleName
        {
            get { return "Otp"; }
        }

        [HttpPost]
        [Route("api/getotp")]
        public async Task<IHttpActionResult> GetotpAsync([FromBody]GetOtpRequest model)
        {
            this.MethodName = "getotp";
            ActionResultStatus responseData = await OtpBL.GetOtpAsync(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [HttpPost]
        [Route("api/confirmotp")]
        public IHttpActionResult Confrimotp([FromBody]ConfrimOtpRequest model)
        {
            this.MethodName = "confrimotp";
            ActionResultStatus responseData = OtpBL.ConfirmOtp(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}
