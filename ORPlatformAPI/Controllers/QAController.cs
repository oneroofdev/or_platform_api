﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORPlatFormAPI.Controllers
{
    public class QAController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "QuestionAnswer";
            }
        }
        [Route("api/getQuestionAll")]
        [HttpGet]
        public IHttpActionResult getQuestionList([FromUri] GetQuestionRequest model)
        {
            this.MethodName = "getQuestionList";
            ActionResultStatus responseData = QABL.getQuestions(model.corpId);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
        [Route("api/getQuestionById")]
        [HttpPost]
        public IHttpActionResult getQuestion([FromBody] QuestionRequest model)
        {
            this.MethodName = "getQuestion";
            ActionResultStatus responseData = QABL.getQuestion(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveQuestion")]
        [HttpPost]
        public IHttpActionResult saveQuestion([FromBody] QuestionRequest model)
        {
            this.MethodName = "saveQuestion";
            ActionResultStatus responseData = QABL.saveQuestion(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveAnswer")]
        [HttpPost]
        public IHttpActionResult saveAnswer([FromBody] QARequest model)
        {
            this.MethodName = "saveAnswer";
            ActionResultStatus responseData = QABL.saveAnswer(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getAnswerByQuestion")]
        [HttpPost]
        public IHttpActionResult getAnswer([FromBody] QARequest model)
        {
            this.MethodName = "getAnswer";
            ActionResultStatus responseData = QABL.getAnswer(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}