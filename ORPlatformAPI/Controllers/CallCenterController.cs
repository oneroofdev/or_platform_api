﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;


namespace ORPlatFormAPI.Controllers
{
    public class CallCenterController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "CallCenter";
            }
        }
        [Route("api/getcallchannel")]
        [HttpPost]
        public IHttpActionResult getChannel([FromBody]GetCallChannelRequest model)
        {
            this.MethodName = "getcallchannel";
            ActionResultStatus responseData = CallCenterBL.getCallChannel(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getquestiontype")]
        [HttpPost]
        public IHttpActionResult getQuestionType([FromBody]GetQuestionTypeRequest model)
        {
            this.MethodName = "getquestiontype";
            ActionResultStatus responseData = CallCenterBL.getQuestionType(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getquestion")]
        [HttpPost]
        public IHttpActionResult getQuestion([FromBody]GetQuestionRequest model)
        {
            this.MethodName = "getquestion"; 
            ActionResultStatus responseData = CallCenterBL.getQuestion(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getanswer")]
        [HttpPost]
        public IHttpActionResult getAnswer([FromBody]GetAnswerRequest model)
        {
            this.MethodName = "getanswer";
            ActionResultStatus responseData = CallCenterBL.getAnswer(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/gethistorycall")]
        [HttpPost]
        public IHttpActionResult getHistoryCall([FromBody]GetHistoryCallRequest model)
        {
            this.MethodName = "gethistorycall";
            ActionResultStatus responseData = CallCenterBL.getHistoryCall(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/gethistorycalldetail")]
        [HttpPost]
        public IHttpActionResult getHistoryCallDetail([FromBody]DataCallRequest model)
        {
            this.MethodName = "gethistorycalldetail";
            ActionResultStatus responseData = CallCenterBL.getHistoryCallDetail(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/gethistorycalldetailById")]
        [HttpPost]
        public IHttpActionResult getHistoryCallDetailByID([FromBody]DataCallRequest model)
        {
            this.MethodName = "gethistorycalldetailById";
            ActionResultStatus responseData = CallCenterBL.getHistoryCallDetailByID(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getdatacallheader")]
        [HttpPost]
        public IHttpActionResult GetDataCallHeader([FromBody]DataCallRequest model)
        {
            this.MethodName = "getdatacallheader";
            ActionResultStatus responseData = CallCenterBL.GetDataCallHeader(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/savecalltransection")]
        [HttpPost]
        public IHttpActionResult saveCallTransection([FromBody]DataCallRequest model)
        {
            this.MethodName = "savecalltransection";
            ActionResultStatus responseData = CallCenterBL.saveCallTransection(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/savecalltransectionDetail")]
        [HttpPost]
        public IHttpActionResult saveCallTransectionDetail([FromBody]DataCallRequest model)
        {
            this.MethodName = "savecalltransectionDetail";
            ActionResultStatus responseData = CallCenterBL.saveCallTransectionDetail(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        } 


        [Route("api/getdatacall")]
        [HttpPost]
        public IHttpActionResult GetDataCall([FromBody]DataCallSearchRequest model)
        {
            this.MethodName = "getdatacall";
            ActionResultStatus responseData = CallCenterBL.getDataCall(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }


        [Route("api/getbranch")]
        [HttpPost]
        public IHttpActionResult getBranch([FromBody]GetCallChannelRequest model)
        {
            this.MethodName = "getbranch";
            ActionResultStatus responseData = CallCenterBL.getBranch(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}