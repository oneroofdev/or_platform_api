﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;

namespace ORPlatFormAPI.Controllers
{
    public class UserController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "User";
            }
        }
        [Route("api/login")]
        [HttpPost]
        public IHttpActionResult Login([FromBody]LoginRequest model)
        {
            this.MethodName = "login";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.LogIn(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
        [Route("api/changepassword")]
        [HttpPost]
        public IHttpActionResult changePassword([FromBody]ChangePasswordRequest model)
        {
            this.MethodName = "changepassword";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.changePassword(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        } 

        [Route("api/changenewpassword")]
        [HttpPost]
        public IHttpActionResult changeNewPassword([FromBody]ChangePasswordRequest model)
        {
            this.MethodName = "changenewpassword";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.changeNewPassword(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/getuser")]
        [HttpPost]
        public IHttpActionResult getUser([FromBody]LoginRequest model)
        {
            this.MethodName = "getuser";
            UserBL userBL = new UserBL();
            ActionResultStatus responseData = userBL.getUser(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}
