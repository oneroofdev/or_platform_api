﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ORPlatFormAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AddressController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Address";
            }
        }
        [Route("api/getProvinceList")]
        [HttpGet]
        public IHttpActionResult getProvinceList([FromUri] AddressRequest model)
        {
            this.MethodName = "getProvinceList";
            ActionResultStatus responseData = AddressBL.ListProvince();
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
        [Route("api/getDistrictList")]
        [HttpGet]
        public IHttpActionResult getDistrictList([FromUri] AddressRequest model)
        {
            this.MethodName = "getDistrictList";
            ActionResultStatus responseData = AddressBL.ListDistrict(model.provinceCode.ToString());
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
        [Route("api/getSubDistrictList")]
        [HttpGet]
        public IHttpActionResult getSubDistrictList([FromUri] AddressRequest model)
        {
            this.MethodName = "getSubDistrictList";
            ActionResultStatus responseData = AddressBL.ListSubDistrict(model.districtCode.ToString());
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}