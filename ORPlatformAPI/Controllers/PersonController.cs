﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORPlatFormAPI.Controllers
{
    public class PersonController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "Person";
            }
        }
        [Route("api/getTitleList")]
        [HttpGet]
        public IHttpActionResult getTitleList([FromUri] AddressRequest model)
        {
            this.MethodName = "getTitleList";
            ActionResultStatus responseData = PersonBL.ListTitle();
            this.InputRequest(model);
            return this.SendResponse(responseData);
        } 

        [Route("api/getDataPersonSearch")]
        [HttpPost]
        public IHttpActionResult getDataPersonSearch(PersonSearchRequest model)
        {
            this.MethodName = "getDataPersonSearch";
            ActionResultStatus responseData = PersonBL.getDataPersonSearch(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }

        [Route("api/saveMember")]
        [HttpPost]
        public IHttpActionResult saveMember([FromBody] PersonRequest model)
        {
            this.MethodName = "saveMember";
            ActionResultStatus responseData = PersonBL.savePerson(model);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}