﻿using ORApiFramework.Controller;
using ORApiFramework.HTTP.Response;
using ORPlatFormAPI.BussinessLogic;
using ORPlatFormAPI.HTTP.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ORPlatFormAPI.Controllers
{
    public class CarSeriesController : AbApiController
    {
        public override string ModuleName
        {
            get
            {
                return "CarSeries";
            }
        }
        [Route("api/getCarSeriesList")]
        [HttpGet]
        public IHttpActionResult getCarSeriesList()
        {
            this.MethodName = "getCarSeriesList";
            ActionResultStatus responseData = CarsBrandBL.ListCarSeries();
            //this.InputRequest(null);
            return this.SendResponse(responseData);
        }
        [Route("api/getCarSeriesByBrand")]
        [HttpGet]
        public IHttpActionResult getCarSeriesByBrand([FromUri] CarSeriesRequest model)
        {
            this.MethodName = "getCarSeriesByBrand";
            ActionResultStatus responseData = CarsBrandBL.ListCarSeriesByBrand(model.carBrandId);
            this.InputRequest(model);
            return this.SendResponse(responseData);
        }
    }
}