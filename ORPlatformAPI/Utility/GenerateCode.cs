﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace ORPlatFormAPI.Utility
{ 
    public static class GenerateCode
    {
        public static DateTime UNIX_EPOCH { get; private set; }
        private static readonly Random _rng = new Random();
        private static string Char = "";

        public static string RandomTimeCode()
        {

            long iterationNumber = (long)(DateTime.UtcNow - UNIX_EPOCH).TotalSeconds;
            //Here the system converts the iteration number to a byte[]
            byte[] iterationNumberByte = BitConverter.GetBytes(iterationNumber);
            //To BigEndian (MSB LSB)
            //if (BitConverter.IsLittleEndian) Array.Reverse(iterationNumberByte);

            //Hash the userId by HMAC-SHA-1 (Hashed Message Authentication Code)
            byte[] userIdByte = Encoding.ASCII.GetBytes("999");
            HMACSHA1 userIdHMAC = new HMACSHA1(userIdByte, true);
            byte[] hash = userIdHMAC.ComputeHash(iterationNumberByte); //Hashing a message with a secret key

            int offset = hash[hash.Length - 1] & 0xf; //0xf = 15d
            int binary =
                ((hash[offset] & 0x7f) << 24)      //0x7f = 127d
                | ((hash[offset + 1] & 0xff) << 16) //0xff = 255d
                | ((hash[offset + 2] & 0xff) << 8)
                | (hash[offset + 3] & 0xff);

            int password = binary % (int)Math.Pow(10, 6); // Shrink: 6 digits
            return password.ToString(new string('0', 6));
        }

        public static string RandomFormatCode()
        {
            string Type = "aA0";
            int digits = 4;
            char[] buffer = new char[digits];
            string expresions = "[a]";
            string expresionS = "[A]";
            string expresionN = "[0]";
            string expresion_ = "[!@#$^&*()_+]";
            if (Regex.IsMatch(Type, expresions))
            {
                Char += "abcdefghijklmnopqrstuvwxyz";
            }
            if (Regex.IsMatch(Type, expresionS))
            {
                Char += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }
            if (Regex.IsMatch(Type, expresionN))
            {
                Char += "0123456789";
            }
            if (Regex.IsMatch(Type, expresion_))
            {
                Char += "!@#$^&*()_+";
            }
            for (int i = 0; i < digits; i++)
            {
                buffer[i] = Char[_rng.Next(Char.Length)];
            }
            return new string(buffer);
        }
    }
}