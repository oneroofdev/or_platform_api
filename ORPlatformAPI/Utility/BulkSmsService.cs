﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ORPlatFormAPI.Utility
{
  public class BulkSmsService
    {
        private const string _reservedCharacters = "!*'();:@&=+$,/?%#[]";
        private static readonly HttpClient client = new HttpClient();

        readonly string BulkAccount = "";
        readonly string BulkPassword = "";
        readonly string BulkEndpoint = $"https://sc4msg.com/bulksms/SendMessage";
        //readonly string BulkEndpoint = $"https://sc2dmc.net/sc2otp3/SendMessage"; 
        public BulkSmsService(string WebConfigAccount, string WebConfigPassword)
        {
            BulkAccount = ConfigurationManager.AppSettings[WebConfigAccount].ToString();
            BulkPassword = ConfigurationManager.AppSettings[WebConfigPassword].ToString();
        }

        public async Task<StreamReader> SendAsync(string mobileNo, string sms, char language = 'T')
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += new RemoteCertificateValidationCallback(AllwaysGoodCertificate);

                string parameters = $"ACCOUNT={BulkAccount}&PASSWORD={BulkPassword}&MOBILE={mobileNo}&MESSAGE={UrlEncode(sms)}&LANGUAGE={language.ToString().ToUpper()}";
             
                WebRequest webRequest = WebRequest.Create(BulkEndpoint);
                webRequest.Method = "POST";
                webRequest.ContentLength = 42;
                webRequest.ContentType = "application/x-www-form-urlencoded";
                Encoding thaiEnc = Encoding.GetEncoding("iso-8859-11");
                byte[] bytes = thaiEnc.GetBytes(parameters);
                Stream os = null;
                webRequest.ContentLength = bytes.Length; //Count bytes to send 
                os = await webRequest.GetRequestStreamAsync();
                await os.WriteAsync(bytes, 0, bytes.Length); //Send it 
                os.Close();
                WebResponse webResponse = await webRequest.GetResponseAsync();

                if (webResponse == null)
                { return null; }

                StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                return sr;
            }
            catch (Exception)
            {

                throw;
            } 
        }

        private static bool AllwaysGoodCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors policyErrors)
        {
            return true;
        }

        private static string UrlEncode(string value)
        {
            if (String.IsNullOrEmpty(value))
                return String.Empty;

            var sb = new StringBuilder();

            foreach (char @char in value)
            {
                if (_reservedCharacters.IndexOf(@char) == -1)
                    sb.Append(@char);
                else
                    sb.AppendFormat("%{0:X2}", (int)@char);
            }
            return sb.ToString();
        }
    }
}