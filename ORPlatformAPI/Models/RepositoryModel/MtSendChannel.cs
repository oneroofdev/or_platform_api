﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtSendChannel : AbRepository<MT_SEND_CHANNEL>
    {

        public override MT_SEND_CHANNEL GetData(string id)
        {
            var data = (from a in DbContext.MT_SEND_CHANNEL
                        where a.SEND_CHANNEL_ID.ToString() == id
                        select a).FirstOrDefault();
            return data;
        }

        public override List<MT_SEND_CHANNEL> GetDatas(string id)
        {
            throw new NotImplementedException();
        }


    }
}