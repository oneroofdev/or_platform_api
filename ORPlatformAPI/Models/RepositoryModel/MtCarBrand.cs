﻿using ORApiFramework.Model;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCarBrand : AbRepository<MT_CAR_BRAND>
    {
        public override MT_CAR_BRAND GetData(string id)
        {
            throw new NotImplementedException();
        }

        public override List<MT_CAR_BRAND> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<CarBrandsResponse> List()
        {
            return (from a in this.DbContext.MT_CAR_BRAND
                    orderby a.BRAND_NAME
                    select new CarBrandsResponse
                    {
                        carBrandId = a.BRAND_ID,
                        carBrandName = a.BRAND_NAME
                    }).ToList();
        }

        public static implicit operator MtCarBrand(MtTitle v)
        {
            throw new NotImplementedException();
        }
    }
}