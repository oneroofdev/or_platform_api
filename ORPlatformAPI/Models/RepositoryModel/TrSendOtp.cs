﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class TrSendOtp : AbRepository<TR_SEND_OTP>
    {

        public override TR_SEND_OTP GetData(string id)
        {
            throw new NotImplementedException();
        }

        public TR_SEND_OTP GetData(string mobileNo, string userName, string otp, string refCode)
        {
            return (from a in DbContext.TR_SEND_OTP
                    where a.USER_LOGIN_NAME == userName && a.PASS_OTP == otp && a.SEND_TO == mobileNo && a.REF_CODE == refCode
                    select a).FirstOrDefault();
        }

        public TR_SEND_OTP BindData(TR_SEND_OTP data, ActionType actionType)
        {

            if (data == null)
                data = new TR_SEND_OTP();

            if (actionType == ActionType.Add)
            {
                //data.LOG_ID = new Guid();
                data.STATUS_OTP = "W";
                data.PASS_INPUT = null;
                data.OTP_SEND_TIME = DateTime.Now;
                data.CREATE_DATE = DateTime.Now;
                data.UPDATE_DATE = DateTime.Now;
            }
            else
            {
                //data.UPDATE_BY = items.authority.empCode;
                //data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public override List<TR_SEND_OTP> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public bool updateSendOTP(string OTP)
        {
            try
            {
                this.Data.STATUS_OTP = "P";
                this.Data.PASS_INPUT = OTP;//model.otp;
                this.Edit(this.Data.LOG_ID.ToString(), this.Data);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

    }
}