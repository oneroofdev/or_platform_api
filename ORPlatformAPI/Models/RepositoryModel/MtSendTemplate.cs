﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtSendTemplate : AbRepository<MT_SEND_TEMPLATE>
    {

        public override MT_SEND_TEMPLATE GetData(string id)
        {
            MT_SEND_TEMPLATE data = (from a in DbContext.MT_SEND_TEMPLATE
                                     where a.TEMPLATE_ID.ToString() == id
                                     select a).FirstOrDefault();
            return data;
        }

        public override List<MT_SEND_TEMPLATE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }


    }
}