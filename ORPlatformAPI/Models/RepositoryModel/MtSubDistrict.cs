﻿using ORApiFramework.Model;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtSubDistrict : AbRepository<MT_DISTRICT>
    {
        public override MT_DISTRICT GetData(string id)
        {
            throw new NotImplementedException();
        }

        public override List<MT_DISTRICT> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
        public List<SubDistrictResponse> List(string id)
        {
            return (from a in this.DbContext.MT_SUBDISTRICT
                    where a.DISTRICT_CODE.ToString() == id
                    orderby a.DISTRICT_CODE
                    select new SubDistrictResponse
                    {
                        subDistrictCode = a.SUBDISTRICT_CODE,
                        subDistrictName = a.SUBDISTRICT_NAME_TH,
                        subDistrictNameEN = a.SUBDISTRICT_NAME_EN,
                        districtCode = a.DISTRICT_CODE,
                        zipcode = a.ZIPCODE
                    }).ToList();
        }
    }
}