﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtUserLogin : AbRepository<MT_USER>
    {

        public override MT_USER GetData(string id)
        {
            return (from A in DbContext.MT_USER
                    where A.ACTIVE == "Y" && A.USER_ID.ToString() == id
                    select A).FirstOrDefault();
        }
        public MT_USER CheckLogin(string userName, string password, string userType)
        {
            return this.DbContext.MT_USER.Where(a => a.USER_LOGIN_NAME == userName && a.PASSWORD == this.CreateMD5(password)).FirstOrDefault();
        }
        public override List<MT_USER> GetDatas(string id)
        {
            return (from A in DbContext.MT_USER
                    where A.ACTIVE == "Y" && (A.USER_ID.ToString() == id || id == "")
                    select A).ToList();
        }
        public bool updateUserPassword(string password)
        {
            try
            {
                this.Data.PASSWORD = CreateMD5(password);
                this.Data.UPDATE_BY = this.Data.USER_LOGIN_NAME;
                this.Data.UPDATE_DATE = DateTime.Now;
                this.Edit(this.Data.USER_ID.ToString(), this.Data);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<MT_USER> GetDatasCorp(  int corpId)
        {
            return (from A in DbContext.MT_USER
                    join b in DbContext.MT_EMPLOYEE on A.EMP_ID equals b.EMP_ID
                    where A.ACTIVE == "Y" && b.CORP_ID == corpId
                    select A).ToList();
        }

        public MT_USER GetDataByMobile(string userName, string mobileNo)
        {
            return (from a in DbContext.MT_USER
                    join b in DbContext.MT_EMPLOYEE on a.EMP_ID equals b.EMP_ID
                    where a.USER_LOGIN_NAME == userName && b.MOBILE == mobileNo
                    select a).FirstOrDefault();
        }
        public string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}