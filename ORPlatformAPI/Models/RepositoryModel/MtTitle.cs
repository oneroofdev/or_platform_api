﻿using ORApiFramework.Model;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtTitle : AbRepository<MT_TITLE>
    {
        public override MT_TITLE GetData(string id)
        {
            throw new NotImplementedException();
        }

        public override List<MT_TITLE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<TitlePresonResponse> List()
        {
            return (from a in this.DbContext.MT_TITLE
                    orderby a.TITLE_ID
                    select new TitlePresonResponse
                    {
                        titleName = a.TITLE_NAME_TH,
                        titleNameEN = a.TITLE_NAME_EN,
                        titleId = a.TITLE_ID,
                        titleGender = a.GENDER
                    }).ToList();

        }
    }
}