﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCallAnswer : AbRepository<MT_CALL_ANSWER>
    {
        public override MT_CALL_ANSWER GetData(string id)
        {
            return (from A in DbContext.MT_CALL_ANSWER
                    where A.ANSWER_ID.ToString() == id 
                    select A).FirstOrDefault();
        }
        public override List<MT_CALL_ANSWER> GetDatas(string questionId)
        {
            return (from A in DbContext.MT_CALL_ANSWER
                    where A.QUESTION_ID.ToString() == questionId 
                    select A).ToList();
        }
        public MT_CALL_ANSWER Save(AnswerRequest request, string questionId, string actionBy, string corpId)
        {

            this.Data = this.GetData(request.answerId);

            if (this.Data == null)
            {
                this.Data = this.BindDataSaveQuestion(request, this.Data, questionId, actionBy, corpId, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSaveQuestion(request, this.Data, questionId, actionBy, corpId, ActionType.Edit);
                this.Edit(request.answerId, this.Data);
            }

            return this.Data;
        }

        public MT_CALL_ANSWER BindDataSaveQuestion(AnswerRequest model, MT_CALL_ANSWER data, string questionId, string actionBy, string corpId, ActionType action)
        {
            if (data == null)
                data = new MT_CALL_ANSWER();

            data.ACTIVE = model.active;
            data.QUESTION_ID = int.Parse(questionId);
            data.ANSWER_DESC = model.answerDesc;
            data.FOLLOW_CASE = model.isFollowCase;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = actionBy;
                data.CREATE_DATE = DateTime.Now;
                data.CORP_ID = int.Parse(corpId);
            }
            else
            {
                data.UPDATE_BY = actionBy;
                data.UPDATE_DATE = DateTime.Now;
            }
            return data;
        }

    }
}