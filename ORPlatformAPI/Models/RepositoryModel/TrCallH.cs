﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class TrCallH : AbRepository<TR_CALL_H>
    {
        public override TR_CALL_H GetData(string id)
        {
            return (from a in this.DbContext.TR_CALL_H
                    where a.TR_CALL_H_ID.ToString() == id
                    select a).FirstOrDefault();
        }
        public override List<TR_CALL_H> GetDatas(string questionId)
        {
            throw new NotImplementedException();
        }
        public string getMaxID_H()
        {
            string st_maxID = "";
            var maxID = DbContext.TR_CALL_H.OrderByDescending(a => a.TR_CALL_H_ID).Take(1).SingleOrDefault();
            if (maxID == null)
            {
                st_maxID = "T000001";
            }
            else
            {
                int temID = Convert.ToInt32(maxID.TR_CALL_H_ID.ToString().Replace("T", "")) + 1;
                st_maxID = "T" + temID.ToString().PadLeft(6, '0');
            }
            return st_maxID;
        }

        public TR_CALL_H Save(DataCallRequest request)
        {

            this.Data = this.GetData(request.trCallHId);

            if (this.Data == null)
            {
                this.Data = this.BindDataSaveTranCall(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSaveTranCall(request, this.Data, ActionType.Edit);
                this.Edit("", this.Data);
            }

            return this.Data;
        }
        public TR_CALL_H BindDataSaveTranCall(DataCallRequest dataBind, TR_CALL_H data, ActionType action)
        {
            if (data == null)
                data = new TR_CALL_H();

            data.CORP_ID = dataBind.corpId;
            data.CALLER_ID = dataBind.callerId;
            data.CALLER_TYPE = dataBind.callerType;
            data.CONTACT_NAME = dataBind.contactName;
            data.CONTACT_PHONE = dataBind.contactPhone;
            data.FINISH_CALL = dataBind.finishCase;
            data.RECEIVE_DATE = DateTime.Now;
            data.RECEIVE_BY = dataBind.userName;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = dataBind.userName;
                data.CREATE_DATE = DateTime.Now;
                data.TR_CALL_H_ID = getMaxID_H();
            }
            else
            {
                data.UPDATE_BY = dataBind.userName;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public void updateFinishCase(string updateBy)
        {
            var row = DbContext.TR_CALL_D.Where(a => a.TR_CALL_H_ID == this.Data.TR_CALL_H_ID).Where(a => a.FINISH_CASE == "N").Count();
            this.Data.FINISH_CALL = (row == 0) ? "Y" : "N";
            this.Data.UPDATE_DATE = DateTime.Now;
            this.Data.UPDATE_BY = updateBy;
            DbContext.SaveChanges();
        }


       
    }
}