﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCallQuestion : AbRepository<MT_CALL_QUESTION>
    { 
        public override MT_CALL_QUESTION GetData(string id)
        {
            return (from A in DbContext.MT_CALL_QUESTION
                    where A.QUESTION_ID.ToString() == id
                    select A).FirstOrDefault();
        }
        public override List<MT_CALL_QUESTION> GetDatas(string questionTypeId)
        {
            return (from A in DbContext.MT_CALL_QUESTION
                    where A.ACTIVE == "Y" && A.QUESTION_TYPE_ID.ToString() == questionTypeId
                    select A).ToList();
        }

        //public List<MT_CALL_QUESTION> GetDatasActiveByQuestionType(string questionTypeId)
        //{
        //    return (from a in DbContext.MT_CALL_QUESTION
        //            where a.ACTIVE == "Y" && a.QUESTION_TYPE_ID.ToString() == questionTypeId
        //            select a).ToList();
        //}
        public MT_CALL_QUESTION Save(QuestionRequest request)
        {

            this.Data = this.GetData(request.questionId);

            if (this.Data == null)
            {
                this.Data = this.BindDataSaveQuestion(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSaveQuestion(request, this.Data, ActionType.Edit);
                this.Edit(request.questionId, this.Data);
            }

            return this.Data;
        }

        public MT_CALL_QUESTION BindDataSaveQuestion(QuestionRequest model, MT_CALL_QUESTION data, ActionType action)
        {
            if (data == null)
                data = new MT_CALL_QUESTION();

            data.ACTIVE = model.isActive;
            data.IS_BRANCH = model.isBranch; 
            data.QUESTION_TYPE_ID = int.Parse(model.questionTypeId);
            data.QUESTION_DESC = model.questionDesc;
            
            if (action == ActionType.Add)
            {
                data.CREATE_BY = model.actionBy;
                data.CREATE_DATE = DateTime.Now;
                data.CORP_ID = int.Parse(model.corpId);
            }
            else
            {
                data.UPDATE_BY = model.actionBy;
                data.UPDATE_DATE = DateTime.Now;
            }
            return data;
        }
    }
}