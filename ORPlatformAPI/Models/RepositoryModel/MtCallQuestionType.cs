﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCallQuestionType : AbRepository<MT_CALL_QUESTION_TYPE>
    {
        public override MT_CALL_QUESTION_TYPE GetData(string id)
        {
            return (from A in DbContext.MT_CALL_QUESTION_TYPE
                    where A.ACTIVE == "Y" && A.QUESTION_TYPE_ID.ToString() == id
                    select A).FirstOrDefault();
        }
        public override List<MT_CALL_QUESTION_TYPE> GetDatas(string id)
        {
            return (from A in DbContext.MT_CALL_QUESTION_TYPE
                    where A.ACTIVE == "Y"  
                    select A).ToList();
        }

        public List<MT_CALL_QUESTION_TYPE> List(string id, string corpId)
        {
            return (from A in DbContext.MT_CALL_QUESTION_TYPE
                    where A.ACTIVE == "Y" && A.CORP_ID.ToString() == corpId
                    select A).ToList();
        }
    }
}