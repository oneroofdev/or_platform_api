﻿using ORApiFramework.Model;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCarSeries : AbRepository<MT_CAR_SERIES>
    {
        public override MT_CAR_SERIES GetData(string id)
        {
            return (from a in this.DbContext.MT_CAR_SERIES
                    where a.SERIES_ID.ToString() == id
                    select a).FirstOrDefault();
        }

        public override List<MT_CAR_SERIES> GetDatas(string id)
        {
            return (from a in this.DbContext.MT_CAR_SERIES
                    where a.BRAND_ID.ToString() == id select a).ToList();
        }

        public  List<CarSeriesResponse> List()
        {
            return (from a in this.DbContext.MT_CAR_SERIES
                    join b in this.DbContext.MT_CAR_BRAND on a.BRAND_ID equals b.BRAND_ID
                    where a.ACTIVE == "Y"
                    select new CarSeriesResponse
                    {
                        carBrandId = a.BRAND_ID,
                        carBrandName = b.BRAND_NAME,
                        carSeriesId = a.SERIES_ID.ToString(),
                        carSeriesName = a.SERIES_NAME
                    }).ToList();
        }
    }
}