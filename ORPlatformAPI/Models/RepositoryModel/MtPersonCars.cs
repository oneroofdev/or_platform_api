﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtPersonCars : AbRepository<MT_PERSON_CARS>
    {
        public override MT_PERSON_CARS GetData(string id)
        {
            return (from a in this.DbContext.MT_PERSON_CARS
                    where a.TRAN_ID.ToString() == id
                    select a).FirstOrDefault();
        }

        public override List<MT_PERSON_CARS> GetDatas(string id)
        {
            return (from a in this.DbContext.MT_PERSON_CARS
                    where a.PERSON_ID.ToString() == id
                    select a).ToList();
        }
        public MT_PERSON_CARS Save(PersonCarsRequest request, string personId, string actiobBy)
        {

            this.Data = this.GetData((string.IsNullOrEmpty(request.tranId) ? "0" : request.tranId));

            if (this.Data == null)
            {
                this.Data = this.BindDataSavePersonCars(request, this.Data, personId, actiobBy, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSavePersonCars(request, this.Data, personId, actiobBy, ActionType.Edit);
                this.Edit(request.tranId, this.Data);
            }

            return this.Data;
        }
        public MT_PERSON_CARS BindDataSavePersonCars(PersonCarsRequest model, MT_PERSON_CARS data, string personId, string actionBy, ActionType action)
        {
            if (data == null)
                data = new MT_PERSON_CARS();


            data.ACTIVE = model.active;
            data.BRAND_NAME = model.carBrandName;
            data.PERSON_ID = int.Parse(personId);
            data.REGISTER_NO = model.register;
            data.SERIE = model.series;
            data.PROVINCE_CODE = (String.IsNullOrEmpty(model.carProvince)) ? (int?)null : int.Parse(model.carProvince);
            data.YEAR = model.carYear;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = actionBy;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = actionBy;
                data.UPDATE_DATE = DateTime.Now;
            }
            return data;
        }

        public List<PersonCarResponse> getDataCars(string id)
        {
            try
            {

                return (from a in this.DbContext.MT_PERSON_CARS
                        where a.PERSON_ID.ToString() == id
                        select new PersonCarResponse
                        {
                            regisNo = a.REGISTER_NO,
                            series = a.SERIE,
                            brandName = a.BRAND_NAME,
                            tranId = a.TRAN_ID,
                            personId = a.PERSON_ID,
                            carYear = a.YEAR,
                            carProvince = a.PROVINCE_CODE.Value
                        }).ToList();
            }
            catch (Exception ex)
            {

                throw new NotImplementedException();
            }
        }


        public List<PersonCarResponse> getDataCarsSearchegisNo(string regisNo)
        {
            return (from a in this.DbContext.MT_PERSON_CARS
                    where a.REGISTER_NO.Contains(regisNo)
                    select new PersonCarResponse
                    {
                        personId = a.PERSON_ID,
                        regisNo = a.REGISTER_NO,
                        series = a.SERIE,
                        brandName = a.BRAND_NAME
                    }).ToList();
        }
    }
}