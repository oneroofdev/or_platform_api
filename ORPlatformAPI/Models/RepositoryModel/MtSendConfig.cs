﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtSendConfig : AbRepository<MT_SEND_CONFIG>
    {

        public override MT_SEND_CONFIG GetData(string id)
        {
            throw new NotImplementedException();
        }
        public MT_SEND_CONFIG GetData()
        {
            this.Data = (from a in DbContext.MT_SEND_CONFIG
                         where a.ACTIVE == "Y"
                         select a).FirstOrDefault();
            return this.Data;
        }
        public override List<MT_SEND_CONFIG> GetDatas(string id)
        {
            throw new NotImplementedException();
        }


    }
}