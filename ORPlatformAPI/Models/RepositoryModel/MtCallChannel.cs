﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCallChannel : AbRepository<MT_CALL_CHANNEL>
    {
        public override MT_CALL_CHANNEL GetData(string id)
        {
            return (from A in DbContext.MT_CALL_CHANNEL
                    where A.ACTIVE == "Y" && A.CHANNEL_ID.ToString() == id
                    select A).FirstOrDefault();
        }
        public override List<MT_CALL_CHANNEL> GetDatas(string id)
        {
            return (from A in DbContext.MT_CALL_CHANNEL
                    where A.ACTIVE == "Y"
                    select A).ToList();
        }
    }
}