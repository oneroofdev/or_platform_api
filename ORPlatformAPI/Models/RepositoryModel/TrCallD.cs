﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class TrCallD : AbRepository<TR_CALL_D>
    {
        public override TR_CALL_D GetData(string id)
        {
            throw new NotImplementedException();
        }
        public override List<TR_CALL_D> GetDatas(string questionId)
        {
            throw new NotImplementedException();
        }

        public string getMaxID_D(String hID)
        {
            string st_maxID = "";
            var maxID = DbContext.TR_CALL_D.Where(a => a.TR_CALL_H_ID == hID).OrderByDescending(a => a.TR_CALL_D_ID).Take(1).SingleOrDefault();
            if (maxID == null)
            {
                st_maxID = "1";
            }
            else
            {
                st_maxID = (Convert.ToInt32(maxID.TR_CALL_D_ID) + 1).ToString();
            }
            return st_maxID;
        }

        public List<DataCallHistoryResponse> getHistoryCall(string callID, string isFinishCase, int corpId)
        {
            return (from cd in DbContext.TR_CALL_D
                    join ch in DbContext.TR_CALL_H on cd.TR_CALL_H_ID equals ch.TR_CALL_H_ID into ch_join
                    from ch in ch_join.DefaultIfEmpty()
                    join q in DbContext.MT_CALL_QUESTION on cd.QUESTION_ID equals q.QUESTION_ID into q_join
                    from q in q_join.DefaultIfEmpty()
                    join bh in DbContext.MT_BRANCH on cd.BRANCH_ID equals bh.BRANCH_ID into bh_join
                    from bh in bh_join.DefaultIfEmpty()
                    where
                        ch.CALLER_ID == callID && ch.DELETE_DATE == null && cd.DELETE_DATE == null && (
                        isFinishCase == null || isFinishCase == "" || cd.FINISH_CASE == isFinishCase) && ch.CORP_ID == corpId
                    orderby
                        cd.CREATE_DATE descending
                    select new DataCallHistoryResponse
                    {
                        trCallHId = cd.TR_CALL_H_ID,
                        trCallDId = cd.TR_CALL_D_ID,
                        finishCase = cd.FINISH_CASE,
                        callerId = ch.CALLER_ID,
                        contactName = ch.CONTACT_NAME,
                        contactPhone = ch.CONTACT_PHONE,
                        receiveDate = ch.RECEIVE_DATE,
                        receiveBy = ch.RECEIVE_BY,
                        finishCall = ch.FINISH_CALL,
                        username = ch.RECEIVE_BY,
                        questionDesc = q.QUESTION_DESC,
                        branchName = bh.BRANCH_NAME_TH,
                        branchId = bh.BRANCH_ID,
                        area = bh.AREA
                    }).ToList();
        }
        public List<DataCallHistoryDetailResponse> getHistoryCallDetail(string trCallHID)
        {
            return (from cd in DbContext.TR_CALL_D
                    join q in DbContext.MT_CALL_QUESTION on cd.QUESTION_ID equals q.QUESTION_ID into q_join
                    from q in q_join.DefaultIfEmpty()
                    join a in DbContext.MT_CALL_ANSWER on cd.ANSWER_ID equals a.ANSWER_ID into a_join
                    from a in a_join.DefaultIfEmpty()
                    join bh in DbContext.MT_BRANCH on cd.BRANCH_ID equals bh.BRANCH_ID into bh_join
                    from bh in bh_join.DefaultIfEmpty()
                    where
                      cd.TR_CALL_H_ID == trCallHID && cd.DELETE_DATE == null
                    orderby
                      cd.TR_CALL_D_ID descending
                    select new DataCallHistoryDetailResponse
                    {
                        trCallDId = cd.TR_CALL_D_ID,
                        finishCase = cd.FINISH_CASE,
                        finishDate = cd.FINISH_DATE,
                        questionDesc = q.QUESTION_DESC,
                        answerDesc = a.ANSWER_DESC,
                        followCase = cd.FOLLOW_CASE,
                        seriousCase = cd.SERIOUS_CASE,
                        remarkDet = cd.REMARK_DET,
                        branchId = bh.BRANCH_ID,
                        branchName = bh.BRANCH_NAME_TH,
                        area = bh.AREA
                    }).ToList();
        }
        public DataCallHistoryDetailByIDResponse getHistoryCallDetailByID(string trCalHId, string trCalDId, int? corpId)
        {
            return (from cd in DbContext.TR_CALL_D
                    join q in DbContext.MT_CALL_QUESTION on cd.QUESTION_ID equals q.QUESTION_ID into q_join
                    from q in q_join.DefaultIfEmpty()
                    join ch in DbContext.TR_CALL_H on cd.TR_CALL_H_ID equals ch.TR_CALL_H_ID into ch_join
                    from ch in ch_join.DefaultIfEmpty()
                    join qt in DbContext.MT_CALL_QUESTION_TYPE on q.QUESTION_TYPE_ID equals qt.QUESTION_TYPE_ID into qt_join
                    from qt in qt_join.DefaultIfEmpty()
                    join bh in DbContext.MT_BRANCH on cd.BRANCH_ID equals bh.BRANCH_ID into bh_join
                    from bh in bh_join.DefaultIfEmpty()
                    where
                      ch.CORP_ID == corpId &&
                      cd.TR_CALL_H_ID == trCalHId &&
                      cd.TR_CALL_D_ID == trCalDId && ch.DELETE_DATE == null && cd.DELETE_DATE == null
                    orderby
                      cd.TR_CALL_D_ID
                    select new DataCallHistoryDetailByIDResponse
                    {
                        trCallDId = cd.TR_CALL_D_ID,
                        trCallHId = cd.TR_CALL_H_ID,
                        contactName = ch.CONTACT_NAME,
                        contactPhone = ch.CONTACT_PHONE,
                        questionTypeId = qt.QUESTION_TYPE_ID,
                        questionId = cd.QUESTION_ID,
                        answerId = cd.ANSWER_ID,
                        remarkDet = cd.REMARK_DET,
                        contactChannel = cd.CONTACT_CHANEL,
                        followCase = cd.FOLLOW_CASE,
                        seriousCase = cd.SERIOUS_CASE,
                        finishCase = cd.FINISH_CASE,
                        branchId = bh.BRANCH_ID,
                        area = bh.AREA,
                        branchName = bh.BRANCH_NAME_TH
                    }).FirstOrDefault();
        }
        public List<DataSearchCallResponse> getTranCall(DataCallSearchRequest model)
        {
            string receive = model.userRecieve;
            int qr1 = (model.questionType == null) ? 0 : model.questionType;
            string dates = "";
            string datee = "";
            // string qr4 = model.cus;
            string qr5 = model.followCase;
            string qr6 = model.seriousCase;
            string qr7 = model.finishCase;

            if (model.startDate != "" && model.startDate != null)
            {
                string dts = model.startDate;
                var datespilt_s = dts.Split('/');
                dates = datespilt_s[2] + "-" + datespilt_s[1] + "-" + datespilt_s[0];
            }
            if (model.endDate != "" && model.endDate != null)
            {
                string dt = model.endDate;
                var datespilt_e = dt.Split('/');
                datee = datespilt_e[2] + "-" + datespilt_e[1] + "-" + datespilt_e[0];
            }
            string txt_where = "";

            if (model.corpId != null)
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE ch.CORP_ID = '" + model.corpId + "' ";
                }
                else
                {
                    txt_where += " AND ch.CORP_ID = '" + model.corpId + "' ";
                }
            }
            if (!String.IsNullOrEmpty(receive))
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE ch.RECEIVE_BY = '" + receive + "' ";
                }
                else
                {
                    txt_where += " AND ch.RECEIVE_BY = '" + receive + "' ";
                }
            }
            if (qr1 != 0)
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE Que.QUESTION_TYPE_ID = " + qr1 + " ";
                }
                else
                {
                    txt_where += " AND Que.QUESTION_TYPE_ID = " + qr1 + " ";
                }
            }
            if (dates != "" && datee != "")
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE CONVERT(VARCHAR(10),RECEIVE_DATE,120) BETWEEN '" + dates + "' AND  '" + datee + "'  ";
                }
                else
                {
                    txt_where += " AND CONVERT(VARCHAR(10),RECEIVE_DATE,120) BETWEEN '" + dates + "'  AND '" + datee + "' ";
                }
            }
            if (!String.IsNullOrEmpty(qr5))
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE cd.FOLLOW_CASE = '" + qr5 + "' ";
                }
                else
                {
                    txt_where += " AND cd.FOLLOW_CASE = '" + qr5 + "' ";
                }
            }
            if (!String.IsNullOrEmpty(qr6))
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE cd.SERIOUS_CASE = '" + qr6 + "' ";
                }
                else
                {
                    txt_where += " AND cd.SERIOUS_CASE = '" + qr6 + "' ";
                }
            }
            if (!String.IsNullOrEmpty(qr7))
            {
                if (txt_where == "")
                {
                    txt_where = " WHERE cd.FINISH_CASE = '" + qr7 + "' ";
                }
                else
                {
                    txt_where += " AND cd.FINISH_CASE = '" + qr7 + "' ";
                }
            }
            string strSQL = @"WITH cte_call AS(
                                SELECT 
                                    cd.TR_CALL_D_ID,
                                    cd.TR_CALL_H_ID,
                                    cd.FINISH_CASE,
                                    cd.FOLLOW_CASE,
                                    cd.QUESTION_ID,
                                    cd.SERIOUS_CASE,
                                    que.QUESTION_TYPE_ID,
                                    que.QUESTION_DESC QUESTION_DESC,
                                    cd.ANSWER_ID,
                                    ch.CONTACT_NAME,
                                    ch.CALLER_ID,
                                    ch.CALLER_TYPE,
                                    ch.CONTACT_PHONE,
                                    ch.RECEIVE_DATE,
                                    ch.RECEIVE_BY,
                                    cd.FINISH_DATE,
									bh.BRANCH_ID,
									bh.BRANCH_NAME_TH,
									bh.AREA
                                FROM TR_CALL_D cd
                                    Left join TR_CALL_H ch ON cd.TR_CALL_H_ID = ch.TR_CALL_H_ID
                                    LEFT JOIN MT_CALL_QUESTION que ON cd.QUESTION_ID = que.QUESTION_ID 
                                    LEFT JOIN MT_BRANCH bh ON cd.BRANCH_ID = bh.BRANCH_ID
                                " + txt_where +
                            @")
                                ,cte_detail AS( 
				                SELECT 
						                cd.TR_CALL_D_ID,
						                cd.TR_CALL_H_ID,
						                cd.FINISH_CASE,
						                cd.FOLLOW_CASE,
						                cd.QUESTION_ID,
						                cd.SERIOUS_CASE,
						                cd.QUESTION_TYPE_ID,
						                cd.QUESTION_DESC,
						                ans.ANSWER_DESC ANSWER_DESC,
						                cd.CONTACT_NAME,
						                cd.CALLER_ID,
						                cd.CALLER_TYPE,
						                cd.CONTACT_PHONE,
						                m.FNAME_TH+ ' ' + LNAME_TH as  NAME,  
						                cd.RECEIVE_DATE,
						                cd.RECEIVE_BY, 
						                m.PERSON_CODE,
						                cd.FINISH_DATE,
                                        cd.BRANCH_ID,
                                        cd.BRANCH_NAME_TH,
                                        cd.AREA
				                FROM cte_call cd
						                LEFT JOIN MT_CALL_ANSWER ans ON cd.ANSWER_ID = ans.ANSWER_ID 
						                LEFT JOIN MT_PERSON m ON cd.CALLER_ID = m.PERSON_ID 
                                )
                                SELECT 
                                    CAST( cd.TR_CALL_D_ID AS VARCHAR(100)) as  trCallDId ,
                                    CAST( cd.TR_CALL_H_ID AS VARCHAR(100)) as trCallHId ,
                                    CAST( cd.FINISH_CASE  AS VARCHAR(100)) as  finishCall ,
                                    CAST( cd.FOLLOW_CASE AS VARCHAR(100)) as followCase  ,
                                    CAST( cd.QUESTION_ID AS VARCHAR(100)) as questionId  ,
                                    CAST( cd.SERIOUS_CASE AS VARCHAR(100)) as seriousCase  ,
                                    CAST( cd.QUESTION_TYPE_ID AS VARCHAR(100)) as questionTypeId  ,
                                    CAST( cd.QUESTION_DESC AS VARCHAR(100)) as questionDesc  ,
                                    CAST(  cd.ANSWER_DESC AS VARCHAR(100)) as answerDesc  ,
                                    CAST( cd.CONTACT_NAME AS VARCHAR(100)) as contactName  ,
                                    CAST( cd.CALLER_ID AS VARCHAR(100)) as callerId  , 
                                    CAST( cd.CONTACT_PHONE AS VARCHAR(100)) as contactPhone  ,
                                    CAST(  cd.NAME  AS VARCHAR(100)) as  name ,  
                                    CAST( CONVERT(VARCHAR(10),cd.RECEIVE_DATE,120) AS VARCHAR(100)) as receiveDate  ,
                                    CAST( cd.RECEIVE_BY AS VARCHAR(100)) as receiveBy  , 
                                    CAST( cd.PERSON_CODE AS VARCHAR(100)) as personCode  ,
                                    CAST( CONVERT(VARCHAR(10),cd.FINISH_DATE,120)  AS VARCHAR(100))  as  finishDate  ,
                                    CAST( cd.BRANCH_ID AS VARCHAR(200)) as branchId  , 
                                    CAST( cd.BRANCH_NAME_TH AS VARCHAR(200)) as branchName  , 
                                    CAST( cd.AREA AS VARCHAR(200)) as area 
                                FROM 
                                    cte_detail cd
                                ORDER BY 
                                    cd.TR_CALL_H_ID DESC,
                                    cd.TR_CALL_D_ID DESC";
            using (var db = DbContext)
            {
                var data = db.Database.SqlQuery<DataSearchCallResponse>(strSQL).ToList();
                List<DataSearchCallResponse> dataCallHistoryDetail = new List<DataSearchCallResponse>();
                foreach (var item in data)
                {
                    dataCallHistoryDetail.Add(new DataSearchCallResponse
                    {
                        trCallHId = item.trCallHId,
                        trCallDId = item.trCallDId,
                        finishCase = item.finishCase,
                        callerId = item.callerId,
                        contactName = item.contactName,
                        contactPhone = item.contactPhone,
                        receiveDate = item.receiveDate,
                        receiveBy = item.receiveBy,
                        // finishCall = item.finishCall,
                        seriousCase = item.seriousCase,
                        followCase = item.followCase,
                        finishCall = item.finishCall,
                        questionId = item.questionId,
                        questionTypeId = item.questionTypeId,
                        questionDesc = item.questionDesc,
                        answerDesc = item.answerDesc,
                        name = item.name,
                        finishDate = item.finishDate,
                        personCode = item.personCode,
                        branchId = item.branchId,
                        branchName = item.branchName,
                        area = item.area
                    });
                }
                return dataCallHistoryDetail;
            }
        }
        public TR_CALL_D Save(DataCallRequest request)
        {
            this.Data = DbContext.TR_CALL_D.Where(x => x.TR_CALL_D_ID == request.trCallDId && x.TR_CALL_H_ID == request.trCallHId).FirstOrDefault();

            if (this.Data == null)
            {
                this.Data = this.BindDataSaveTranCall(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSaveTranCall(request, this.Data, ActionType.Edit);
                this.Edit("", this.Data);
            }
            TrCallH trCallH = new TrCallH();
            trCallH.updateFinishCase(request.userName);
            return this.Data;
        }
        public TR_CALL_D BindDataSaveTranCall(DataCallRequest dataBind, TR_CALL_D data, ActionType action)
        {
            if (data == null)
                data = new TR_CALL_D();

            data.TR_CALL_H_ID = dataBind.trCallHId;
            data.QUESTION_ID = dataBind.questionId;
            data.ANSWER_ID = dataBind.answerId;
            data.CONTACT_CHANEL = dataBind.contactChannel;
            data.REMARK_DET = dataBind.remarkDet;
            data.SERIOUS_CASE = dataBind.seriousCase;
            data.FOLLOW_CASE = dataBind.followCase;
            data.FINISH_CASE = dataBind.finishCase; 
            data.BRANCH_ID = dataBind.branch;
            data.FINISH_BY = (dataBind.finishCase == "Y") ? dataBind.userName : (string)null;
            data.FINISH_DATE = (dataBind.finishCase == "Y") ? DateTime.Now : (DateTime?)null;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = dataBind.userName;
                data.CREATE_DATE = DateTime.Now;
                data.TR_CALL_D_ID = getMaxID_D(data.TR_CALL_H_ID);
            }
            else
            {
                data.UPDATE_BY = dataBind.userName;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

    }
}

