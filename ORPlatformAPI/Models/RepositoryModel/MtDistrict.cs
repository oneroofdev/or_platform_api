﻿using ORApiFramework.Model;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtDistrict : AbRepository<MT_DISTRICT>
    {
        public override MT_DISTRICT GetData(string id)
        {
            throw new NotImplementedException();
        }

        public override List<MT_DISTRICT> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<DistrictResponse> List(string id)
        {
            return (from a in this.DbContext.MT_DISTRICT
                    where a.PROVINCE_CODE.ToString() == id
                    orderby a.DISTRICT_CODE
                    select new DistrictResponse
                    {
                        districtCode = a.DISTRICT_CODE,
                        districtName = a.DISTRICT_NAME_TH,
                        districtNameEN = a.DISTRICT_NAME_EN,
                        provinceCode = a.PROVINCE_CODE
                    }).ToList();
        }
    }
}