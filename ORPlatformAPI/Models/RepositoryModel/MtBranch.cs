﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtBranch : AbRepository<MT_BRANCH>
    {
        public override MT_BRANCH GetData(string id)
        {
            return (from A in DbContext.MT_BRANCH
                    where A.ACTIVE == "Y" && A.BRANCH_ID.ToString() == id
                    select A).FirstOrDefault();
        }
        public override List<MT_BRANCH> GetDatas(string id)
        {
            return (from A in DbContext.MT_BRANCH
                    where A.ACTIVE == "Y"
                    select A).ToList();
        }
    }
}