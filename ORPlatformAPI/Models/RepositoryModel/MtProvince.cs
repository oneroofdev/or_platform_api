﻿using ORApiFramework.Model;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtProvince : AbRepository<MT_PROVINCE>
    {
        public override MT_PROVINCE GetData(string id)
        {
            return (from a in this.DbContext.MT_PROVINCE
                    where a.PROVINCE_CODE.ToString() == id
                    select a).FirstOrDefault();
            //throw new NotImplementedException();
        }

        public override List<MT_PROVINCE> GetDatas(string id)
        {
            //return (from a in this.DbContext.MT_PROVINCE
            //        orderby a.PROVINCE_CODE
            //        select a).ToList();
            throw new NotImplementedException();
        }

        public List<ProvinceResponse> List()
        {
            return (from a in this.DbContext.MT_PROVINCE
                    orderby a.PROVINCE_CODE
                    select new ProvinceResponse
                    {
                        provinceCode = a.PROVINCE_CODE,
                        provinceName = a.PROV_NAME_TH,
                        provinceNameEN = a.PROV_NAME_EN
                    }).ToList();
        }

    }
}