﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtPersonAddress : AbRepository<MT_PERSON_ADDRESS>
    {
        public static CultureInfo cultureInfo = new CultureInfo("en-US");
        public static string format = "dd/MM/yyyy";
        public override MT_PERSON_ADDRESS GetData(string id)
        {
            return (from a in this.DbContext.MT_PERSON_ADDRESS
                    where a.TRAN_ID.ToString() == id
                    select a).FirstOrDefault();
        }

        public override List<MT_PERSON_ADDRESS> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public MT_PERSON_ADDRESS Save(PersonRequest request)
        {

            this.Data = this.GetData(string.IsNullOrEmpty(request.addrTranId) ? "0" : request.addrTranId);

            if (this.Data == null)
            {
                this.Data = this.BindDataSavePersonAddress(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSavePersonAddress(request, this.Data, ActionType.Edit);
                this.Edit(request.addrTranId, this.Data);
            }

            return this.Data;
        }
        public MT_PERSON_ADDRESS BindDataSavePersonAddress(PersonRequest model, MT_PERSON_ADDRESS data, ActionType action)
        {
            if (data == null)
                data = new MT_PERSON_ADDRESS();


            data.ADDR_NO = model.addressNo;
            data.ADDR_TYPE = model.addressType;
            data.VILLAGE = model.village;
            data.DISTRICT_CODE = (String.IsNullOrEmpty(model.districtId)) ? (int?)null : int.Parse(model.districtId);
            data.FLOOR = model.floor;
            data.MOO = model.moo;
            data.PERSON_ID = int.Parse(model.personId);
            data.PROVINCE_CODE = (String.IsNullOrEmpty(model.provinceId)) ? (int?)null : int.Parse(model.provinceId);  //int.Parse(model.provinceId);
            data.ROAD = model.road;
            data.SOI = model.soi;
            data.SUBDISTRICT_CODE = (String.IsNullOrEmpty(model.subdistrictId)) ? (int?)null : int.Parse(model.subdistrictId); //int.Parse(model.subdistrictId);
            data.ZIPCODE = model.zipcode;
            data.BUILDING_NAME = model.buildingName;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = model.actionBy;
                data.CREATE_DATE = DateTime.Now;
            }
            else
            {
                data.UPDATE_BY = model.actionBy;
                data.UPDATE_DATE = DateTime.Now;
            }
            return data;
        }
    }
}