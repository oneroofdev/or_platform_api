﻿using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtPerson : AbRepository<MT_PERSON>
    {
        public static CultureInfo cultureInfo = new CultureInfo("en-US");
        public static string format = "dd/MM/yyyy";
        public override MT_PERSON GetData(string id)
        {
            return (from a in this.DbContext.MT_PERSON
                    where a.PERSON_ID.ToString() == id
                    select a).FirstOrDefault();
        }

        public override List<MT_PERSON> GetDatas(string id)
        {
            throw new NotImplementedException();
        }

        public List<PersonResponse> getDataSearchPerson(int corpId, int personId, string textSearch, string typeSearch)
        {
            try
            {
                MtPersonCars mtPersonCars = new MtPersonCars();
                int[] dataListRegis = { };
                // var dataRegis = mtPersonCars.getDataCarsSearchegisNo(textSearch).Select(x => x.personId).ToArray();
                dataListRegis = mtPersonCars.getDataCarsSearchegisNo(textSearch).Select(x => x.personId).ToArray();
                List<PersonResponse> personResponse = new List<PersonResponse>();
                var data = (from a in this.DbContext.MT_PERSON
                            join b in DbContext.MT_TITLE on a.TITLE_ID equals b.TITLE_ID
                            join c in DbContext.MT_PERSON_ADDRESS on a.PERSON_ID equals c.PERSON_ID into ad
                            from ads in ad.DefaultIfEmpty()
                            join d in DbContext.MT_PROVINCE on ads.PROVINCE_CODE equals d.PROVINCE_CODE into pv
                            from pvs in pv.DefaultIfEmpty()
                            join e in DbContext.MT_DISTRICT on ads.DISTRICT_CODE equals e.DISTRICT_CODE into dt
                            from dts in dt.DefaultIfEmpty()
                            join f in DbContext.MT_SUBDISTRICT on ads.SUBDISTRICT_CODE equals f.SUBDISTRICT_CODE into sdt
                            from sdts in sdt.DefaultIfEmpty()
                            where ((typeSearch == "I" && a.PERSON_ID == personId) ||
                                  (typeSearch == "C" && (dataListRegis.Contains(a.PERSON_ID) || a.COMPANY_NAME.Contains(textSearch) || a.FNAME_TH.Contains(textSearch) || a.MOBILE.Contains(textSearch) || a.LNAME_TH.Contains(textSearch))))
                                  && a.CORP_ID == corpId
                            orderby a.FNAME_TH
                            select new PersonResponse
                            {
                                personId = a.PERSON_ID,
                                personCode = a.PERSON_CODE,
                                gender = a.GENDER,
                                fullName = a.FNAME_TH + " " + a.LNAME_TH,
                                firstName = a.FNAME_TH,
                                lastName = a.LNAME_TH,
                                titileId = a.TITLE_ID,
                                titileName = b.TITLE_NAME_TH,
                                idCard = a.IDCARD,
                                passport = a.PASSPORT,
                                birthDate = a.BIRTHDAY,
                                mobilePhone = a.MOBILE,
                                mobilePhoneSMS = a.MOBILE_SMS,
                                addrTranId = (ads.TRAN_ID == null) ? (int?)null : ads.TRAN_ID,
                                addrType = ads.ADDR_TYPE,
                                addrNo = ads.ADDR_NO,
                                buildingName = ads.BUILDING_NAME,
                                floor = ads.FLOOR,
                                village = ads.VILLAGE,
                                moo = ads.MOO,
                                soi = ads.SOI,
                                road = ads.ROAD,
                                subDistinctCode = ads.SUBDISTRICT_CODE,
                                subDistinctName = sdts.SUBDISTRICT_NAME_TH,
                                distinctCode = ads.DISTRICT_CODE,
                                distinctName = dts.DISTRICT_NAME_TH,
                                provinceCode = ads.PROVINCE_CODE,
                                provinceName = pvs.PROV_NAME_TH,
                                zipCode = ads.ZIPCODE,
                                companyName = a.COMPANY_NAME,
                                lineId = a.LINEID,
                                email = a.EMAIL,
                                memberTier = a.MEMBER_TIER,
                                priority = a.PRIORITY,
                                regisDate = a.CREATE_DATE,
                                personType = a.CUST_TYPE,
                                phone = a.PHONE
                                // cars = mtPersonCars.getDataCars(a.PERSON_ID.ToString()),
                            }).ToList();
                foreach (var items in data)
                {
                    var dataCars = mtPersonCars.getDataCars(items.personId.ToString());
                    string dataCarsText = "";
                    foreach (var itemsCar in dataCars)
                    {
                        if (dataCarsText != "")
                        {
                            dataCarsText += " ,";
                        }
                        dataCarsText += itemsCar.regisNo;// + "[" + itemsCar.brandName + "]";
                        if (itemsCar.carProvince != null)
                        {
                            MtProvince pv = new MtProvince();
                            dataCarsText += " จ." + pv.GetData(itemsCar.carProvince.ToString()).PROV_NAME_TH;
                        }
                        if (!String.IsNullOrEmpty(itemsCar.carYear))
                        {
                            dataCarsText += " ปี" + itemsCar.carYear;
                        }
                        if (!String.IsNullOrEmpty(itemsCar.brandName))
                        {
                            if (!String.IsNullOrEmpty(itemsCar.series))
                            {
                                dataCarsText += "[" + itemsCar.brandName + " " + itemsCar.series + "]";

                            }
                            else
                            {
                                dataCarsText += "[" + itemsCar.brandName + "]";

                            }
                        }
                    }
                    personResponse.Add(new PersonResponse
                    {
                        personId = items.personId,
                        personCode = items.personCode,
                        gender = items.gender,
                        fullName = items.fullName,
                        firstName = items.firstName,
                        lastName = items.lastName,
                        titileId = items.titileId,
                        titileName = items.titileName,
                        idCard = items.idCard,
                        passport = items.passport,
                        birthDate = items.birthDate,
                        mobilePhone = items.mobilePhone,
                        mobilePhoneSMS = items.mobilePhoneSMS,
                        addrTranId = items.addrTranId,
                        addrType = items.addrType,
                        addrNo = items.addrNo,
                        buildingName = items.buildingName,
                        floor = items.floor,
                        village = items.village,
                        moo = items.moo,
                        soi = items.soi,
                        road = items.road,
                        subDistinctCode = items.subDistinctCode,
                        subDistinctName = items.subDistinctName,
                        distinctCode = items.distinctCode,
                        distinctName = items.distinctName,
                        provinceCode = items.provinceCode,
                        provinceName = items.provinceName,
                        zipCode = items.zipCode,
                        companyName = items.companyName,
                        lineId = items.lineId,
                        email = items.email,
                        memberTier = items.memberTier,
                        priority = items.priority,
                        regisDate = items.regisDate,
                        cars = dataCars,
                        carsText = dataCarsText,
                        personType = items.personType,
                        phone = items.phone
                    });
                }
                return personResponse;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException();
            }
        }

        public MT_PERSON Save(PersonRequest request)
        {

            this.Data = this.GetData(request.personId);

            if (this.Data == null)
            {
                this.Data = this.BindDataSavePerson(request, this.Data, ActionType.Add);
                this.Create(this.Data);
            }
            else
            {
                this.Data = this.BindDataSavePerson(request, this.Data, ActionType.Edit);
                this.Edit(request.personId, this.Data);
            }

            return this.Data;
        }
        public MT_PERSON BindDataSavePerson(PersonRequest dataBind, MT_PERSON data, ActionType action)
        {
            if (data == null)
                data = new MT_PERSON();


            data.BIRTHDAY = String.IsNullOrEmpty(dataBind.birthday) ? (DateTime?)null : DateTime.ParseExact(dataBind.birthday, format, cultureInfo);
            data.CHANNEL = dataBind.channel;
            data.CORP_ID = int.Parse(dataBind.corpId);
            //data.CORP_PERSON_ID = !string.IsNullOrEmpty(dataBind.corpPersonId) ? int.Parse(dataBind.corpPersonId) : 0;
            data.EMAIL = dataBind.email;
            data.FNAME_TH = dataBind.fnameTh;
            data.GENDER = dataBind.gender;
            data.IDCARD = dataBind.idcard;
            data.LINEID = dataBind.lineId;
            data.LNAME_TH = dataBind.lnameTh;
            data.MEMBER_TIER = dataBind.memberTier;
            data.MOBILE = dataBind.mobile;
            data.MOBILE_SMS = dataBind.mobileSMS;
            data.PASSPORT = dataBind.passport;
            data.PRIORITY = string.IsNullOrEmpty(dataBind.priority) ? null : dataBind.priority;
            data.TITLE_ID = int.Parse(dataBind.titleId);
            data.CUST_TYPE = dataBind.personType;
            data.COMPANY_NAME = dataBind.companyName;
            data.PHONE = dataBind.phone;

            if (action == ActionType.Add)
            {
                data.CREATE_BY = dataBind.actionBy;
                data.CREATE_DATE = DateTime.Now;
                data.PERSON_CODE = genPersonCode();
            }
            else
            {
                data.UPDATE_BY = dataBind.actionBy;
                data.UPDATE_DATE = DateTime.Now;
            }

            return data;
        }

        public string genPersonCode()
        {
            int running = 1;
            string personCode = "";
            var format = DateTime.Now.ToString("yyMM");

            var data = (from a in this.DbContext.MT_PERSON
                        where a.PERSON_CODE.Contains(format)
                        orderby a.PERSON_CODE descending
                        select a).FirstOrDefault();

            if (data != null)
            {
                running = int.Parse(data.PERSON_CODE.Substring(data.PERSON_CODE.Length - 4, 4)) + 1;
            }
            personCode = format + running.ToString("0000");

            return personCode;
        }
    }
}