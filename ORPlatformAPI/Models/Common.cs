﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Helpers;
using System.Web.Hosting;
using DB_MODEL;

namespace INCENTIVE_API.Models
{
  
    public class Common : System.Web.Http.ApiController
    {
        public class SendEmail
        {
            public string mailBody { get; set; }

            [EmailAddress(ErrorMessage = "The mailCC address is not valid")]
            public string mailCC { get; set; }

            [EmailAddress(ErrorMessage = "The mailBCC address is not valid")]
            public string mailBCC { get; set; }

            [Required(ErrorMessage = "The mailTo is required.")]
            [EmailAddress(ErrorMessage = "The mailTo address is not valid")]
            public string mailTo { get; set; }

            [Required(ErrorMessage = "The contentType is required.")]
            public string contentType { get; set; }

            public string mailSubject { get; set; }

            [Required(ErrorMessage = "The channel is required.")]
            public string channel { get; set; }
        }

        public class SendSMS
        {
            [Required(ErrorMessage = "The mobileNumber is required.")]
            public string mobileNumber { get; set; }

            [Required(ErrorMessage = "The channel is required.")]
            [Display(Name = "channel")]

            public string channel { get; set; }

            public string message { get; set; }
        }
    }
  
    public class Files_Manage
    {
        public string UploadFile(HttpPostedFileBase image, string fileName)
        {
            int ImageSizeLimit = 2000;
            if (Path.GetExtension(image.FileName).ToLower() != ".jpg")
            {
                throw new Exception("The uploaded file type is not .jpg");
            }
            WebImage img = new WebImage(image.InputStream);
            if (img.Width > ImageSizeLimit)
            {
                img.Resize(ImageSizeLimit, 1500, true, true);
            }
            string newFullPath = HostingEnvironment.MapPath("~/Images/") + fileName;
            img.Save(newFullPath, null, false);
             return newFullPath;
        }
        protected void DeleteImage(string filename)
        {
            string fullFilePath = HostingEnvironment.MapPath("~/Images/") + filename;
            if (System.IO.File.Exists(fullFilePath))
            {
                System.IO.File.Delete(fullFilePath);
            }
            //db.FilePaths.Remove(filePath);
            //db.SaveChanges();
        }
    }

    public class Convertor {
       public string NullToString(object Value)
        {
            return Value == null ? "-" : Value.ToString();
        }
        public string NullToStringZero(object Value)
        {
            return Value == null ? "0" : Value.ToString();
        }
    }
    

}