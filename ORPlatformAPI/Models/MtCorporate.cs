﻿using ORApiFramework.Model;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ORPlatFormAPI.Models.RepositoryModel
{
    public class MtCorporate : AbRepository<MT_CORPORATE>
    {
        public override MT_CORPORATE GetData(string id)
        {
            return (from A in DbContext.MT_CORPORATE
                    where A.ACTIVE == "Y" && A.CORP_ID.ToString() == id
                    select A).FirstOrDefault();
        }

        public override List<MT_CORPORATE> GetDatas(string id)
        {
            throw new NotImplementedException();
        }
    }
}