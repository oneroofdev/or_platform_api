﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.BussinessLogic
{
    public class AddressBL : AbBLRepository
    {

        public static ActionResultStatus ListProvince()
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtProvince province = new MtProvince();
                var data = province.List();

                mReturn.data = data;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }
            return mReturn;
        }
        public static ActionResultStatus ListDistrict(string id)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtDistrict district = new MtDistrict();
                var data = district.List(id);
                mReturn.data = data;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }
            return mReturn;
        }
        public static ActionResultStatus ListSubDistrict(string id)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtSubDistrict subdistrict = new MtSubDistrict();
                var data = subdistrict.List(id);
                mReturn.data = data;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }
            return mReturn;
        }
    }
}