﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormAPI.Models.RepositoryModel;
using ORPlatFormAPI.Utility;
using ORPlatFormModel.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ORPlatFormAPI.BussinessLogic
{
    public class OtpBL : AbBLRepository
    {

        public static async Task<ActionResultStatus> GetOtpAsync(GetOtpRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            GetOtpResponse result = new GetOtpResponse();
            try
            {
                MtUserLogin mtUserLogin = new MtUserLogin();
                mtUserLogin.Data = mtUserLogin.GetDataByMobile(model.userName, model.mobileNo);

                if (mtUserLogin.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบข้อมูล สมาชิก");
                }

                // ค้นหา config
                MtSendConfig mtSendConfig = new MtSendConfig();
                mtSendConfig.Data = mtSendConfig.GetData();
                if (mtSendConfig.Data == null)
                {
                    //ไม่พบข้อมูล mtSendConfig
                    return MessageUtility.ThrowErrorMessage(model, "ไม่สามารถส่ง OTP ได้");
                }
                // ค้นหา template, channel จาก config
                MtSendTemplate mtSendTemplate = new MtSendTemplate();
                mtSendTemplate.Data = mtSendTemplate.GetData(mtSendConfig.Data.TEMPLATE_ID.ToString());
                if (mtSendTemplate.Data == null)
                {
                    //ไม่พบข้อมูล mtSendTemplate
                    return MessageUtility.ThrowErrorMessage(model, "ไม่สามารถส่ง OTP ได้");
                }
                MtSendChannel mtSendChannel = new MtSendChannel();
                mtSendChannel.Data = mtSendChannel.GetData(mtSendConfig.Data.SEND_CHANNEL_ID.ToString());
                if (mtSendChannel.Data == null)
                {
                    //ไม่พบข้อมูล mtSendChanel
                    return MessageUtility.ThrowErrorMessage(model, "ไม่สามารถส่ง OTP ได้");
                }

                string otp = GenerateCode.RandomTimeCode();
                string refCode = GenerateCode.RandomFormatCode();

                string smsTemplate = mtSendTemplate.Data.TEMPLATE_TEXT;
                smsTemplate = smsTemplate.Replace("(OTP)", otp);
                smsTemplate = smsTemplate.Replace("(REF_CODE)", refCode);
                smsTemplate = smsTemplate.Replace("(TIME)", mtSendConfig.Data.DEFINE_TIME_OUT.ToString());
                string sms = smsTemplate;

                // START Send SMS
                BulkSmsService bulkSmsService = new BulkSmsService("BulkAccount", "BulkPassword");
                StreamReader sr = await bulkSmsService.SendAsync(model.mobileNo, sms);
                if (sr == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่สามารถส่ง SMS ได้");
                }
                // END Send SMS
                TrSendOtp trSendOtp = new TrSendOtp();

                TR_SEND_OTP trSendOtpData = trSendOtp.BindData(null, ActionType.Add);
                trSendOtpData.USER_LOGIN_NAME = model.userName;
                trSendOtpData.REF_CODE = refCode;
                trSendOtpData.PASS_OTP = otp;
                trSendOtpData.SEND_TO = model.mobileNo;
                trSendOtpData.OTP_TIME_OUT = DateTime.Now.AddMinutes(mtSendConfig.Data.DEFINE_TIME_OUT);
                trSendOtpData.TEMPLATE_ID = mtSendConfig.Data.TEMPLATE_ID;
                trSendOtpData.SEND_CHANNEL_ID = mtSendConfig.Data.SEND_CHANNEL_ID;

                trSendOtp.Create(trSendOtpData);

                // set result
                result.refCode = refCode;

                // set response
                actionResultStatus.data = result;
                // for test
                actionResultStatus.otherData = new
                {
                    otp,
                    sms,
                    tel = model.mobileNo,
                    input = model,
                    smsProvider = sr.ReadToEnd().Trim()
                };

            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = result;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }

            return actionResultStatus;

        }

        public static ActionResultStatus ConfirmOtp(ConfrimOtpRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            ConfrimOtpResponse result = new ConfrimOtpResponse();
            try
            {
                TrSendOtp trSendOtp = new TrSendOtp();
                trSendOtp.Data = trSendOtp.GetData(model.mobileNo, model.userName, model.otp, model.refCode);
                if (trSendOtp.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ OTP นี้");
                }
                if (trSendOtp.Data.OTP_TIME_OUT < DateTime.Now)
                {
                    result.expired = true;
                    actionResultStatus.data = result;
                    MessageUtility.ThrowErrorMessage(model, "OTP หมดอายุ");
                    return actionResultStatus;
                }

                MtUserLogin mtUserLogin = new MtUserLogin();
                mtUserLogin.Data = mtUserLogin.GetDataByMobile(model.userName, model.mobileNo);
                if (mtUserLogin.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบข้อมูล vwMember");
                }
                trSendOtp.updateSendOTP(model.otp);

                //Data.STATUS_OTP = "P";
                //trSendOtp.Data.PASS_INPUT = model.otp;
                //trSendOtp.Edit(trSendOtp.Data.LOG_ID.ToString(), trSendOtp.Data);

                result.userId = mtUserLogin.Data.USER_ID.ToString();
                result.expired = false;

                actionResultStatus.data = result;
            }
            catch (Exception ex)
            {
                actionResultStatus.code = (int)StatusCode.NotSave;
                actionResultStatus.data = result;
                actionResultStatus.message = ex.Message;
                actionResultStatus.description = ex.ErrorException();
            }

            return actionResultStatus;
        }
    }
}