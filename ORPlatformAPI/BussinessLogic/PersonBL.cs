﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.BussinessLogic
{
    public class PersonBL : AbBLRepository
    {
        public static ActionResultStatus ListTitle()
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtTitle title = new MtTitle();
                var data = title.List();
                mReturn.data = data;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }
            return mReturn;
        }
        public static ActionResultStatus getDataPersonSearch(PersonSearchRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtPerson mtPerson = new MtPerson();
                var data = mtPerson.getDataSearchPerson(model.corpId, model.personId, model.textSearch, model.typeSearch);
                mReturn.data = data;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }
            return mReturn;
        }
        public static ActionResultStatus savePerson(PersonRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            var dbContext = new ORPlatFormModelContext();
            using (var dbContextTransaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    ////MtNewsType mtNewsType = new MtNewsType(ref dbContext);
                    //mtNewsType.Data = mtNewsType.Save(request);
                    MtPerson person = new MtPerson();
                    var data = person.Save(model);
                    model.personId = data.PERSON_ID.ToString();
                    MtPersonAddress addr = new MtPersonAddress();
                    if (!String.IsNullOrEmpty(model.addressNo)
                        || !String.IsNullOrEmpty(model.village)
                        || !String.IsNullOrEmpty(model.floor)
                        || !String.IsNullOrEmpty(model.moo)
                        || !String.IsNullOrEmpty(model.road)
                        || !String.IsNullOrEmpty(model.soi)
                        || !String.IsNullOrEmpty(model.provinceId)
                        || !String.IsNullOrEmpty(model.districtId)
                        || !String.IsNullOrEmpty(model.subdistrictId)
                        || !String.IsNullOrEmpty(model.zipcode)
                        )
                    {
                        var dataAddr = addr.Save(model);
                    }
                    MtPersonCars cars = new MtPersonCars();
                    foreach (var item in model.personCars)
                    {
                        if (!String.IsNullOrEmpty(item.carBrandName)
                            || !String.IsNullOrEmpty(item.register))
                        {
                            cars.Save(item, model.personId, model.actionBy);
                        }
                        
                    }
                    dbContextTransaction.Commit();

                    //result.success = true;
                    //result.key = mtNewsType.Data.NEWS_TYPE_ID.ToString();


                    mReturn.data = data.PERSON_ID.ToString();

                }
                catch (DbEntityValidationException e)
                {
                    dbContextTransaction.Rollback();
                    mReturn.code = (int)StatusCode.NotSave;
                    mReturn.success = false;
                    mReturn.message = e.ErrorException();
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                            mReturn.message += ve.ErrorMessage;
                        }
                    }

                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    mReturn.code = (int)StatusCode.NotSave;
                    mReturn.success = false;
                    mReturn.message = ex.ErrorException();
                }

            }
            return mReturn;
        }

    }
}