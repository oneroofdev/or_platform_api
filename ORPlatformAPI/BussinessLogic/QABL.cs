﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.BussinessLogic
{
    public class QABL : AbBLRepository
    {
        public static ActionResultStatus getQuestions(int corpId)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallQuestion mtCallQuestion = new MtCallQuestion();
                MtCallQuestionType mtCallQuestionType = new MtCallQuestionType();
                List<QuestionResponse> questionResponse = new List<QuestionResponse>();
                var data = mtCallQuestion.GetAll().Where(r => r.CORP_ID == corpId).ToList();
                if (data.Count <= 0)
                {
                    return MessageUtility.ThrowErrorMessage(null, "ไม่พบ ข้อมูลคำถาม");
                }
                else
                {
                    foreach (var items in data)
                    {
                        questionResponse.Add(new QuestionResponse
                        {
                            questionId = items.QUESTION_ID,
                            questionDesc = items.QUESTION_DESC,
                            questionTypeId = items.QUESTION_TYPE_ID,
                            questionTypeDesc = mtCallQuestionType.GetData(items.QUESTION_TYPE_ID.ToString()).QUESTION_TYPE_DESC,
                            questionType = items.TYPE,
                            active = items.ACTIVE,
                            createBy = items.CREATE_BY,
                            createDate = items.CREATE_DATE,
                            type = items.TYPE,
                            updateBy = items.UPDATE_BY,
                            updateDate = items.UPDATE_DATE
                        });
                    }
                }

                mReturn.data = questionResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus getQuestion(QuestionRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallQuestion mtCallQuestion = new MtCallQuestion();
                QuestionResponse questionResponse = new QuestionResponse();
                var data = mtCallQuestion.GetData(model.questionId);
                if (data == null)
                {
                    return MessageUtility.ThrowErrorMessage(null, "ไม่พบ ข้อมูลคำถาม");
                }
                else
                {
                    questionResponse = new QuestionResponse
                    {
                        questionId = data.QUESTION_ID,
                        questionDesc = data.QUESTION_DESC,
                        questionTypeId = data.QUESTION_TYPE_ID,
                        //questionTypeDesc = mtCallQuestionType.GetData(data.QUESTION_TYPE_ID.ToString()).QUESTION_TYPE_DESC,
                        active = data.ACTIVE,
                        isBranch = data.IS_BRANCH,
                        createBy = data.CREATE_BY,
                        createDate = data.CREATE_DATE,
                        type = data.TYPE,
                        updateBy = data.UPDATE_BY,
                        updateDate = data.UPDATE_DATE
                    };
                }
                mReturn.data = questionResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus saveQuestion(QuestionRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            var dbContext = new ORPlatFormModelContext();
            using (var dbContextTransaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    ////MtNewsType mtNewsType = new MtNewsType(ref dbContext);
                    //mtNewsType.Data = mtNewsType.Save(request);
                    MtCallQuestion question = new MtCallQuestion();
                    var data = question.Save(model);

                    dbContextTransaction.Commit();

                    //result.success = true;
                    //result.key = mtNewsType.Data.NEWS_TYPE_ID.ToString();


                    mReturn.data = data;

                }
                catch (DbEntityValidationException e)
                {
                    dbContextTransaction.Rollback();
                    mReturn.code = (int)StatusCode.NotSave;
                    mReturn.success = false;
                    mReturn.message = e.ErrorException();
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                            mReturn.message += ve.ErrorMessage;
                        }
                    }

                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    mReturn.code = (int)StatusCode.NotSave;
                    mReturn.success = false;
                    mReturn.message = ex.ErrorException();
                }

            }
            return mReturn;
        }
        public static ActionResultStatus saveAnswer(QARequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            var dbContext = new ORPlatFormModelContext();
            using (var dbContextTransaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    MtCallAnswer answer = new MtCallAnswer();
                    foreach (var item in model.answer)
                    {
                        var data = answer.Save(item, model.questionId, model.actionBy, model.corpId);
                    }

                    dbContextTransaction.Commit();

                    //mReturn.data = data;

                }
                catch (DbEntityValidationException e)
                {
                    dbContextTransaction.Rollback();
                    mReturn.code = (int)StatusCode.NotSave;
                    mReturn.success = false;
                    mReturn.message = e.ErrorException();
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                            mReturn.message += ve.ErrorMessage;
                        }
                    }

                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    mReturn.code = (int)StatusCode.NotSave;
                    mReturn.success = false;
                    mReturn.message = ex.ErrorException();
                }

            }
            return mReturn;
        }
        public static ActionResultStatus getAnswer(QARequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallAnswer answer = new MtCallAnswer();
                List<AnswerResponse> answerResponse = new List<AnswerResponse>();
                var data = answer.GetDatas(model.questionId);
                if (data.Count <= 0)
                {
                    return MessageUtility.ThrowErrorMessage(null, "ไม่พบ ข้อมูลคำถาม");
                }

                foreach (var items in data)
                {
                    answerResponse.Add(new AnswerResponse
                    {
                        answerId = items.ANSWER_ID,
                        questionId = items.QUESTION_ID,
                        answerDesc = items.ANSWER_DESC,
                        followCase = items.FOLLOW_CASE,
                        active = items.ACTIVE
                    });
                }


                mReturn.data = answerResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
    }
}