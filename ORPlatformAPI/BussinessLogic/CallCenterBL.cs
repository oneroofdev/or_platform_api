﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using System;
using System.Data.Common;
using System.Linq;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormAPI.Models.RepositoryModel;
using System.Collections.Generic;

namespace ORPlatFormAPI.BussinessLogic
{
    public class CallCenterBL : AbBLRepository
    {
        public static ActionResultStatus getQuestionType(GetQuestionTypeRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallQuestionType mtCallQuestionType = new MtCallQuestionType();
                mtCallQuestionType.Datas = mtCallQuestionType.GetDatas(null).Where(x => x.CORP_ID == model.corpId).ToList();
                if (mtCallQuestionType.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลประเภทคำถาม");
                }
                List<QuestionTypeResponse> questionTypeResponse = new List<QuestionTypeResponse>();
                foreach (var items in mtCallQuestionType.Datas)
                {
                    questionTypeResponse.Add(new QuestionTypeResponse
                    {
                        questionTypeId = items.QUESTION_TYPE_ID,
                        questionTypeDesc = items.QUESTION_TYPE_DESC
                    });
                }
                mReturn.data = questionTypeResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus getCallChannel(GetCallChannelRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallChannel mtCallChannel = new MtCallChannel();
                mtCallChannel.Datas = mtCallChannel.GetDatas(null);
                if (mtCallChannel.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลช่องทาง");
                }
                List<CallChannelResponse> callChannelResponse = new List<CallChannelResponse>();
                foreach (var items in mtCallChannel.Datas)
                {
                    callChannelResponse.Add(new CallChannelResponse
                    {
                        channelId = items.CHANNEL_ID,
                        channelName = items.CHANNEL_NAME
                    });
                }
                mReturn.data = callChannelResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus getQuestion(GetQuestionRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallQuestion mtCallQuestion = new MtCallQuestion();
                var data = mtCallQuestion.FindBy(x => x.ACTIVE == "Y" && x.QUESTION_TYPE_ID == model.questionType && x.TYPE == model.type && x.CORP_ID == model.corpId);
                if (data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลคำถาม");
                }
                List<QuestionResponse> questionResponse = new List<QuestionResponse>();
                foreach (var items in data)
                {
                    questionResponse.Add(new QuestionResponse
                    {
                        questionId = items.QUESTION_ID,
                        questionDesc = items.QUESTION_DESC,
                        questionTypeId = items.QUESTION_TYPE_ID,
                        questionType = items.TYPE,
                        isBranch = items.IS_BRANCH
                    });
                }
                mReturn.data = questionResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }


        public static ActionResultStatus getAnswer(GetAnswerRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtCallAnswer mtCallAnswer = new MtCallAnswer();
                mtCallAnswer.Datas = mtCallAnswer.GetDatas(model.questionId.ToString()).Where(x => x.ACTIVE == "Y" && x.CORP_ID == model.corpId).ToList();
                if (mtCallAnswer.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลคำตอบ");
                }
                List<AnswerResponse> answerResponse = new List<AnswerResponse>();
                foreach (var items in mtCallAnswer.Datas)
                {
                    answerResponse.Add(new AnswerResponse
                    {
                        answerId = items.ANSWER_ID,
                        questionId = items.QUESTION_ID,
                        answerDesc = items.ANSWER_DESC,
                        followCase = items.FOLLOW_CASE
                    });
                }
                mReturn.data = answerResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus getHistoryCall(GetHistoryCallRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                TrCallD trCallD = new TrCallD();
                mReturn.data = trCallD.getHistoryCall(model.callId.ToString(), model.isFinishCase,model.corpId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public static ActionResultStatus getHistoryCallDetail(DataCallRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                TrCallD trCallD = new TrCallD();
                mReturn.data = trCallD.getHistoryCallDetail(model.trCallHId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }


        public static ActionResultStatus getHistoryCallDetailByID(DataCallRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                TrCallD trCallD = new TrCallD();
                mReturn.data = trCallD.getHistoryCallDetailByID(model.trCallHId, model.trCallDId ,model.corpId);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus saveCallTransection(DataCallRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {

                TrCallH trCallH = new TrCallH();
                var dataCallH = trCallH.Save(model);
                model.trCallHId = dataCallH.TR_CALL_H_ID;
                TrCallD trCallD = new TrCallD();
                var ss = trCallD.Save(model);
                mReturn.data = model.trCallHId;

            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus saveCallTransectionDetail(DataCallRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                TrCallD trCallD = new TrCallD();
                trCallD.Save(model);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public static ActionResultStatus GetDataCallHeader(DataCallRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                TrCallH trCallH = new TrCallH();
                trCallH.Data = trCallH.GetData(model.trCallHId);
                DataCallHistoryResponse dataCallHistoryResponse = new DataCallHistoryResponse();
                dataCallHistoryResponse.receiveDate = trCallH.Data.RECEIVE_DATE;
                dataCallHistoryResponse.receiveBy = trCallH.Data.RECEIVE_BY;
                mReturn.data = dataCallHistoryResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public static ActionResultStatus getDataCall(DataCallSearchRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                TrCallD trCallD = new TrCallD();
                mReturn.data = trCallD.getTranCall(model);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public static ActionResultStatus getBranch(GetCallChannelRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtBranch mtBranch = new MtBranch();
                mtBranch.Datas = mtBranch.GetDatas(null);
                if (mtBranch.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลสาขา");
                }
                List<BranchResponse> branchResponse = new List<BranchResponse>();
                foreach (var items in mtBranch.Datas)
                {
                    branchResponse.Add(new BranchResponse
                    {
                        branchId = items.BRANCH_ID,
                        branchName = items.BRANCH_NAME_TH,
                        area = items.AREA
                    });
                }
                mReturn.data = branchResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

    }
}