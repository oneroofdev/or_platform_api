﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using System;
using System.Data.Common;
using System.Linq;
using ORPlatFormAPI.HTTP.Request;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormAPI.Models.RepositoryModel;
using System.Collections.Generic;

namespace ORPlatFormAPI.BussinessLogic
{
    public class UserBL : AbBLRepository
    {
        public ActionResultStatus LogIn(LoginRequest items)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtUserLogin mtUserLogin = new MtUserLogin();
                string password = mtUserLogin.CreateMD5(items.password);
                var dataLogin = (from a in this.DbContext.MT_USER
                                 join b in this.DbContext.MT_DEPARTMENT on a.DEPT_ID equals b.DEPT_ID into QuesGrp
                                 from b in QuesGrp.DefaultIfEmpty()
                                 join c in this.DbContext.MT_CORPORATE on b.CORP_ID equals c.CORP_ID into CorpGrp
                                 from c in CorpGrp.DefaultIfEmpty()
                                 where a.USER_LOGIN_NAME == items.userName &&
                                        a.PASSWORD == password &&
                                        a.ACTIVE == "Y" && c.ACTIVE == "Y"
                                 select new DataLoginInfo
                                 {
                                     userId = a.USER_ID,
                                     userName = a.USER_LOGIN_NAME,
                                     empId = a.EMP_ID,
                                     corpId = b.CORP_ID,
                                     deptId = b.DEPT_ID,
                                     deptName = b.DEPT_NAME_TH,
                                     isExecutive = b.IS_EXECUTIVE,
                                     corpName = c.CORP_NAME_EN,
                                     corpLogo = c.LOGO_PATH,
                                     dataLoginMenu = (from c in this.DbContext.MT_PROGRAM_PERMISSION_GROUP
                                                      join d in this.DbContext.MT_PROGRAM on c.PROGRAM_ID equals d.PROGRAM_ID into ppGrp
                                                      from d in ppGrp.DefaultIfEmpty()
                                                      join e in this.DbContext.MT_PROGRAM_GROUP on d.GRP_MENU_ID equals e.GRP_MENU_ID into pGrp
                                                      from e in pGrp.DefaultIfEmpty()
                                                      where c.DEPT_ID == b.DEPT_ID && c.ACTIVE == "Y" && d.ACTIVE == "Y"
                                                      orderby d.GRP_MENU_ID ascending
                                                      select new DataLoginMenuInfo
                                                      {
                                                          programId = c.PROGRAM_ID,
                                                          displaySeq = d.DISPLAY_SEQ,
                                                          menuName = d.NAME,
                                                          description = d.DESCRIPTION,
                                                          url = !string.IsNullOrEmpty(d.URL) ? d.URL : "#",
                                                          grpMenuId = d.GRP_MENU_ID,
                                                          grpMenuName = e.GRP_MENU_NAME,
                                                          iconMenu = d.ICON_MENU,
                                                          displayOnPage = e.DISPLAY_ON_PAGE,
                                                          isMain = d.IS_MAIN
                                                      }).ToList()
                                 }).FirstOrDefault();
                var dataMain = (from a in this.DbContext.MT_USER
                                join b in this.DbContext.MT_DEPARTMENT on a.DEPT_ID equals b.DEPT_ID into QuesGrp
                                from b in QuesGrp.DefaultIfEmpty()
                                where a.USER_LOGIN_NAME == items.userName &&
                                       a.PASSWORD == password &&
                                       a.ACTIVE == "Y"
                                select new DataLoginInfo
                                {
                                    userId = a.USER_ID,
                                    empId = a.EMP_ID,
                                    corpId = b.CORP_ID,
                                    deptId = b.DEPT_ID,
                                    deptName = b.DEPT_NAME_TH,
                                    isExecutive = b.IS_EXECUTIVE,
                                    dataLoginMenu = (from c in this.DbContext.MT_PROGRAM_PERMISSION_GROUP
                                                     join d in this.DbContext.MT_PROGRAM on c.PROGRAM_ID equals d.PROGRAM_ID into ppGrp
                                                     from d in ppGrp.DefaultIfEmpty()
                                                     join e in this.DbContext.MT_PROGRAM_GROUP on d.GRP_MENU_ID equals e.GRP_MENU_ID into pGrp
                                                     from e in pGrp.DefaultIfEmpty()
                                                     where c.DEPT_ID == b.DEPT_ID
                                                     orderby d.GRP_MENU_ID ascending
                                                     select new DataLoginMenuInfo
                                                     {
                                                         //programId = c.PROGRAM_ID,
                                                         displaySeq = d.DISPLAY_SEQ,
                                                         menuName = d.NAME,
                                                         //description = d.DESCRIPTION,
                                                         url = !string.IsNullOrEmpty(d.URL) ? d.URL : "#",
                                                         grpMenuId = d.GRP_MENU_ID,
                                                         grpMenuName = e.GRP_MENU_NAME,
                                                         iconMenu = d.ICON_MENU,
                                                         //displayOnPage = e.DISPLAY_ON_PAGE,
                                                         isMain = d.IS_MAIN,
                                                         subMenu = this.DbContext.MT_PROGRAM.Where(r => r.GRP_MENU_ID == e.GRP_MENU_ID).Select(r => new SubManu
                                                         {
                                                             menuName = d.NAME,
                                                             description = d.DESCRIPTION,
                                                             url = !string.IsNullOrEmpty(d.URL) ? d.URL : "#",
                                                             grpMenuId = d.GRP_MENU_ID,
                                                             iconMenu = d.ICON_MENU,
                                                             displayOnPage = e.DISPLAY_ON_PAGE,
                                                             displaySeq = d.DISPLAY_SEQ
                                                         }).ToList()
                                                     }).ToList()
                                }).FirstOrDefault();
                //MtUserLogin mtUserLogin = new MtUserLogin();

                //mtUserLogin.Data = mtUserLogin.CheckLogin(items.userName, items.password, items.userTypeId);

                if (dataLogin == null)
                {
                    mReturn.success = false;
                    mReturn.message = StatusCode.PasswordInCorrect.Value();
                    mReturn.description = StatusCode.PasswordInCorrect.Value();
                    mReturn.code = (int)StatusCode.PasswordInCorrect;
                }
                else
                {
                    mReturn.data = dataLogin;
                }
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
        public ActionResultStatus changePassword(ChangePasswordRequest model)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                MtUserLogin mtUserLogin = new MtUserLogin();
                mtUserLogin.Data = mtUserLogin.GetData(model.userId.ToString());
                if (mtUserLogin.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลสมาชิก");
                }
                mtUserLogin.updateUserPassword(model.password);
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }

        public ActionResultStatus changeNewPassword(ChangePasswordRequest model)
        {
            ActionResultStatus actionResultStatus = new ActionResultStatus();
            try
            {
                MtUserLogin mtUserLogin = new MtUserLogin();
                mtUserLogin.Data = mtUserLogin.GetData(model.userId.ToString());
                if (mtUserLogin.Data == null)
                {
                    return MessageUtility.ThrowErrorMessage(model, "ไม่พบ ข้อมูลสมาชิก");
                }
                else
                {
                    if (mtUserLogin.Data.PASSWORD == mtUserLogin.CreateMD5(model.oldPassword))
                    {
                        mtUserLogin.updateUserPassword(model.password);
                    }
                    else
                    {
                        return MessageUtility.ThrowErrorMessage(model, "รหัสผ่านไม่ถูกต้อง กรุณาตรวจสอบ");
                    }
                }
            }
            catch (Exception ex)
            {
                actionResultStatus.success = false;
                actionResultStatus.message = ex.ErrorException();
                actionResultStatus.code = (int)StatusCode.NotSave;
            }

            return actionResultStatus;
        }

        public ActionResultStatus getUser(LoginRequest items)
        {
            ActionResultStatus mReturn = new ActionResultStatus();
            try
            {
                List<LoginResponse> loginResponse = new List<LoginResponse>();
                MtUserLogin mtUserLogin = new MtUserLogin();
                mtUserLogin.Datas = mtUserLogin.GetDatasCorp(items.corpId);
                foreach (var item in mtUserLogin.Datas)
                {
                    loginResponse.Add(
                        new LoginResponse
                        {
                            userName = item.USER_LOGIN_NAME
                        }
                     );
                }
                mReturn.data = loginResponse;
            }
            catch (Exception ex)
            {
                mReturn.success = false;
                mReturn.message = ex.ErrorException();
                mReturn.code = (int)StatusCode.NotSave;
            }

            return mReturn;
        }
    }
}