﻿using ORApiFramework.HTTP.Response;
using ORApiFramework.Model;
using ORApiFramework.Model.Enum;
using ORApiFramework.Utility;
using ORPlatFormAPI.HTTP.Response;
using ORPlatFormAPI.Models.RepositoryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ORPlatFormAPI.BussinessLogic
{
    public class CarsBrandBL : AbBLRepository
    {
        public static ActionResultStatus ListCarBrand()
        {
            ActionResultStatus myReturn = new ActionResultStatus();
            try
            {
                MtCarBrand brand = new MtCarBrand();
                var data = brand.List();
                myReturn.data = data;
            }
            catch (Exception ex)
            {
                myReturn.success = false;
                myReturn.message = ex.ErrorException();
                myReturn.code = (int)StatusCode.NotSave;
            }
            return myReturn;
        }

        public static ActionResultStatus ListCarSeriesByBrand(string brandId)
        {
            ActionResultStatus myReturn = new ActionResultStatus();
            try
            {
                MtCarSeries series = new MtCarSeries();
                var data = series.GetDatas(brandId);
                var listSeries = new List<CarSeriesResponse>();
                foreach (var item in data)
                {
                    listSeries.Add(new CarSeriesResponse
                    {
                        carBrandId = item.BRAND_ID,
                        carSeriesId = item.SERIES_ID.ToString(),
                        carSeriesName = item.SERIES_NAME
                    });
                }
                myReturn.data = listSeries;
            }
            catch (Exception ex)
            {
                myReturn.success = false;
                myReturn.message = ex.ErrorException();
                myReturn.code = (int)StatusCode.NotSave;
            }
            return myReturn;
        }
        public static ActionResultStatus ListCarSeries()
        {
            ActionResultStatus myReturn = new ActionResultStatus();
            try
            {
                MtCarSeries series = new MtCarSeries();
                var data = series.List();
                myReturn.data = data;
            }
            catch (Exception ex)
            {
                myReturn.success = false;
                myReturn.message = ex.ErrorException();
                myReturn.code = (int)StatusCode.NotSave;
            }
            return myReturn;
        }
    }
}